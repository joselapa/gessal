﻿using System;
using System.Windows.Controls;

namespace GesSal
{
    /// <summary>
    /// Interação lógica para Entrada.xaml
    /// </summary>
    /// 
    public partial class Entrada : Page
    {
        public Entrada()
        {
            InitializeComponent();

            // Uma vez que vai haver um binding para a colocação da imagem 
            // é necessário ativar a propriedade DatContext.
            DataContext = this;
        }

       /// <summary>
       /// Este método é chamado aquando a binding utilizado no user inteface.
       /// </summary>
        public string LocationString
        { 
            // Este getter é para ser utilizado num binding para ir buscar a imagem à pasta partilhada.
            get
            {
                string caminho = "\\\\" + Principal.Janela.DominioGesSala + "\\GesSala\\Imagens\\Compo.png";
                return caminho;
            }
        }
    }
}
