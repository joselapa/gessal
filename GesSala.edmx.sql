
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/24/2019 17:08:29
-- Generated from EDMX file: C:\Projeto\GesSal\GesSal\GesSala.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GesSala];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_equip_fotosEquip]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[fotosEquip] DROP CONSTRAINT [FK_equip_fotosEquip];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[equip]', 'U') IS NOT NULL
    DROP TABLE [dbo].[equip];
GO
IF OBJECT_ID(N'[dbo].[fotosEquip]', 'U') IS NOT NULL
    DROP TABLE [dbo].[fotosEquip];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'equip'
CREATE TABLE [dbo].[equip] (
    [idEquip] int IDENTITY(1,1) NOT NULL,
    [nome] varchar(50)  NULL,
    [modelo] varchar(20)  NULL,
    [comprimento] int  NULL,
    [altura] int  NULL,
    [largura] int  NULL,
    [cor] varchar(20)  NULL,
    [peso] int  NULL,
    [preco] float  NULL,
    [caracteristicas] varchar(max)  NULL,
    [estado] bit  NULL,
    [localizacao] varchar(1)  NULL
);
GO

-- Creating table 'fotosEquip'
CREATE TABLE [dbo].[fotosEquip] (
    [idFotoEquip] int IDENTITY(1,1) NOT NULL,
    [idEquip] int  NULL,
    [fotoPath] varchar(255)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [idEquip] in table 'equip'
ALTER TABLE [dbo].[equip]
ADD CONSTRAINT [PK_equip]
    PRIMARY KEY CLUSTERED ([idEquip] ASC);
GO

-- Creating primary key on [idFotoEquip] in table 'fotosEquip'
ALTER TABLE [dbo].[fotosEquip]
ADD CONSTRAINT [PK_fotosEquip]
    PRIMARY KEY CLUSTERED ([idFotoEquip] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [idEquip] in table 'fotosEquip'
ALTER TABLE [dbo].[fotosEquip]
ADD CONSTRAINT [FK_equip_fotosEquip]
    FOREIGN KEY ([idEquip])
    REFERENCES [dbo].[equip]
        ([idEquip])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_equip_fotosEquip'
CREATE INDEX [IX_FK_equip_fotosEquip]
ON [dbo].[fotosEquip]
    ([idEquip]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------