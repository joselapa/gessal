﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace GesSal.Paginas.Salas
{
    /// <summary>
    ///  Interação lógica para AlteraSala.xaml
    /// </summary>
    public partial class AlteraSala : Page
    {
        // Criação de variável para guardar cliente selecionado.
        int selecao = int.Parse(VerSala.JanelaVerSala.verSelecao);

        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int compCorreto, altCorreta, largCorreta = 1;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public AlteraSala()
        {
            InitializeComponent();

            using (GesSalEntities sacaDados = new GesSalEntities())
            {

                // Preenchimento dos campos de texto.
                var a = sacaDados.salas.Where(x => x.idSala == selecao).FirstOrDefault();
                tbLargura.Text = a.largura.ToString();
                tbComprimento.Text = a.comprimento.ToString();
                tbAltura.Text = a.altura.ToString();
                tbPiso.Text = a.piso;
                tbNome.Text = a.identificacao;
                tbCaracteristicas.Text = a.caracteristicas;

                // Atribuição da lista á combobox.
                var qEdificio = (from c in sacaDados.edificios select c).ToList();
                cbEdificio.ItemsSource = qEdificio;

                // Selecionar a combobox com o valor da base de dados.
                cbEdificio.SelectedIndex = qEdificio.FindIndex(s => s.idEdificio.Equals(a.idEdificio));
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerSala();
        }


        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butOk_Click(object sender, RoutedEventArgs e)
        {

            // Verificação de se os campos obrigatórios estão preenchidos
            if (tbNome.Text == "" ||
                tbAltura.Text == "" ||
                tbComprimento.Text == "" ||
                tbLargura.Text == "" ||
                tbPiso.Text == "" ||
                cbEdificio.SelectedIndex < 1
                )
            {
                MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
            }
            // Verificação de se algums campos estão preenchidos corretamente.
            else if (compCorreto + largCorreta + altCorreta != 3)
            {
                MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
            }
            else
            {

                // Gravação dos dados na base de dados com as condições atrás verificadas.
                using (GesSalEntities db = new GesSalEntities())
                {
                    salas minhaSala = db.salas.Where(x => x.idSala == selecao).FirstOrDefault();
                    minhaSala.comprimento = int.Parse(tbComprimento.Text);
                    minhaSala.largura = int.Parse(tbLargura.Text);
                    minhaSala.altura = int.Parse(tbAltura.Text);
                    minhaSala.piso = tbPiso.Text;
                    minhaSala.identificacao = tbNome.Text;
                    minhaSala.caracteristicas = tbCaracteristicas.Text;
                    minhaSala.idEdificio = ((edificios)cbEdificio.SelectedValue).idEdificio;
                    db.SaveChanges();
                }
                Principal.Janela.AreaTrabalho.Content = new VerSala();
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Eliminar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Pretende mesmo eliminar esta sala?", "A Eliminar...", MessageBoxButton.YesNo).ToString() == "Yes")
            {
                using (GesSalEntities db = new GesSalEntities())
                {
                    List<equip> qEquip = (from c in db.equip
                              where (c.idSala.ToString().Contains(selecao.ToString()))
                              select c).ToList();
                   
                    List<agenda> qAgenda = (from c in db.agenda
                                           where (c.idSala.ToString().Contains(selecao.ToString()))
                                           select c).ToList();

                    // Verificação de se existe algum equipamento atribuido à sala.
                    if (qEquip.Count > 0)
                    {
                        string aviso = "A sala " + tbNome.Text + " não pode ser eliminada pois tem equipamentos atribuidos.";
                        MessageBox.Show(aviso);
                    }
                    else if (qAgenda.Count > 0)
                    {
                        string aviso = "A sala " + tbNome.Text + " não pode ser eliminada pois tem eventos atribuidos.";
                        MessageBox.Show(aviso);
                    }
                    else
                    {

                        //Eliminação do registo caso as condiçõesa assim o permitam.
                        salas aEliminar = db.salas.Where(x => x.idSala == selecao).FirstOrDefault();
                        db.salas.Remove(aEliminar);
                        db.SaveChanges();
                    }
                }
            }
            Principal.Janela.AreaTrabalho.Content = new VerSala();
        }

        // Verificação do preenchimento de alguns campos, se estão na configuração devida ou não, 
        // com a emissão do respetivo aviso. São utilizdas expressões regulares para o efeito
        // sendo o campo verificado logo após o preenchimento.

        /// <summary>
        /// Interação lógica para a ação de abandono de um dos campos Comprimento,
        /// Altura ou Largura.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis a 1 para o caso de ter sido colocada a 0.
            if (textoRec.Name == "tbComprimento")
            { compCorreto = 1; }
            else if (textoRec.Name == "tbAltura")
            { altCorreta = 1; }
            else
            { largCorreta = 1; }
            Regex regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter apenas digitos.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbComprimento")
                { compCorreto = 0; }
                else if (textoRec.Name == "tbAltura")
                { altCorreta = 0; }
                else
                { largCorreta = 0; }
            }
        }

    }

}
