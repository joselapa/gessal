﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace GesSal.Paginas.Salas
{
    /// <summary>
    /// Interação lógica para CriarSala.xaml
    /// </summary>
    public partial class CriarSala : Page
    {
        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int compCorreto, altCorreta, largCorreta = 1;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public CriarSala()
        {
            InitializeComponent();

            // Criação de uma query para o preenchimento da combobox do edifício para a 
            // inserção no campo correspondente.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var qEdificio = (from c in sacaDados.edificios select c).ToList();
                cbEdificio.ItemsSource = qEdificio;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butOk_Click(object sender, RoutedEventArgs e)
        {

            // Verificação de se os campos obrigatórios estão preenchidos
            if (tbNome.Text == "" ||
                tbAltura.Text == "" ||
                tbComprimento.Text == "" ||
                tbLargura.Text == "" ||
                tbPiso.Text == "" ||
                cbEdificio.SelectedIndex < 1
                )
            {
                MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
            }
            // Verificação de se algums campos estão preenchidos corretamente.
            else if (compCorreto + largCorreta + altCorreta != 3)
            {
                MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
            }
            else
            {
                using (GesSalEntities db = new GesSalEntities())
                {
                    salas novaSala = new salas()
                    {
                        largura = float.Parse(tbLargura.Text),
                        comprimento = float.Parse(tbComprimento.Text),
                        altura = float.Parse(tbAltura.Text),
                        piso = tbPiso.Text,
                        identificacao = tbNome.Text,
                        idEdificio = ((edificios)cbEdificio.SelectedValue).idEdificio,
                        caracteristicas = tbCaracteristicas.Text
                    };
                    db.salas.Add(novaSala);
                    db.SaveChanges();
                    Principal.Janela.AreaTrabalho.Content = new Paginas.Salas.VerSala();
                }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerSala();
        }

        // Verificação do preenchimento de alguns campos, se estão na configuração devida ou não, 
        // com a emissão do respetivo aviso. São utilizdas expressões regulares para o efeito
        // sendo o campo verificado logo após o preenchimento.

        /// <summary>
        /// Interação lógica para a ação de abandono de qualquer um dos campos Comprimento,
        /// Altura ou Largura.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis a 1 para o caso de ter sido colocada a 0.
            if (textoRec.Name == "tbComprimento")
            { compCorreto = 1; }
            else if (textoRec.Name == "tbAltura")
            { altCorreta = 1; }
            else
            { largCorreta = 1; }
            Regex regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter apenas digitos.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbComprimento")
                { compCorreto = 0; }
                else if (textoRec.Name == "tbAltura")
                { altCorreta = 0; }
                else
                { largCorreta = 0; }
            }
        }
    }
}
