﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Salas
{
    /// <summary>
    /// Interação lógica para DetalheSala.xaml
    /// </summary>
    public partial class DetalheSala : Page
    {

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public DetalheSala()
        {
            InitializeComponent();

            // Variável para receber a informação do equipamento selecionado.
            string posicao = VerSala.JanelaVerSala.verSelecao;

            using (GesSalEntities sacaDados = new GesSalEntities())
            {

                // Preenchimento dos groupbox com a informação selecionada.
                var s = sacaDados.salas.Where(x => x.idSala.ToString() == posicao).FirstOrDefault();
                gpCaracteristicas.Content = s.caracteristicas;
                gpSala.Content = s.identificacao;
                string edif = s.idEdificio.ToString();
                var e = sacaDados.edificios.Where(x => x.idEdificio.ToString() == edif).FirstOrDefault();
                gpEdificio.Content = e.idetificaco;

                // Preenchimento da datagrid de equipamentos com a informação selecionada.
                var qEquip = (from c in sacaDados.equip
                              where c.idSala.ToString() == posicao
                              select c).ToList();
                dgEquipamento.ItemsSource = qEquip;

                // Preenchimento da datagrid de agenda com a informação selecionada.
                var qAgenda = (from a in sacaDados.agenda
                               where a.idSala.ToString() == posicao
                               select a).ToList();
                dgAgenda.ItemsSource = qAgenda;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Voltar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butVoltar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerSala();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Alterar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butAlterar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new Paginas.Salas.AlteraSala();
        }


    }
}
