﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Salas
{
    /// <summary>
    /// Interação lógica para VerSala.xaml
    /// </summary>
    public partial class VerSala : Page
    {

        // Implementação de uma váriável para identificar a seleção feita pelo utilizador.
        public string verSelecao = "0";

        // Criação de uma variável estática do tipo desta classe para conseguir  
        // ter acesso às propriedades publicas desta classe noutras classes 
        // desta aplicação.
        public static VerSala JanelaVerSala;


        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public VerSala()
        {
            InitializeComponent();

            //Inicialização da variável estática criada atrás.
            JanelaVerSala = this;

            //
            using (GesSalEntities sacaDados = new GesSalEntities())
            {

                //Criação de uma variável para receber os dados da query feita.
                var qSala = (from c in sacaDados.salas
                             join b in sacaDados.edificios
                             on c.idEdificio equals b.idEdificio
                              select new
                             { c.idSala,c.identificacao,c.largura,c.comprimento,c.altura,c.piso,
                                  c.caracteristicas, b.idetificaco
                             }).ToList();

                // Passagem dos dados para a datagrid.
                dgDados.ItemsSource = qSala;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Detalhes".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butDetalhes_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new DetalheSala();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Criar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCriar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new CriarSala();
        }


        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Alterar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butAlterar_Click(object sender, RoutedEventArgs e)
        {

            // Verificação da existência de uma seleção da tabela das salas.
            if (verSelecao == "0")
            {
                MessageBox.Show("Tem de selcionar primeiro uma sala.");
            }
            else
            {
                Principal.Janela.AreaTrabalho.Content = new AlteraSala();
            }
        }

        /// <summary>
        /// Interação lógica para a ação de selecionar um rigisto da datagrid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgDados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var type = dgDados.CurrentItem.GetType();
            verSelecao = ((int)type.GetProperty("idSala").GetValue(dgDados.CurrentItem, null)).ToString();

        }

    }
}