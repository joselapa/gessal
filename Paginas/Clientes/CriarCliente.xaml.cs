﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace GesSal.Paginas.Clientes
{
    /// <summary>
    /// Interação lógica para CriarCliente.xaml
    /// </summary>
    public partial class CriarCliente : Page
    {
        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int telCorreto, movCorreto, mailCorreto, cPostalCorreto = 1;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public CriarCliente()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerCliente();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butOk_Click(object sender, RoutedEventArgs e)
        {
            // Verificação de se os campos obrigatórios estão preenchidos
            if (tbConcelho.Text == "" ||
                tbDistrito.Text == "" ||
                tbLocalidade.Text == "" ||
                tbMorada.Text == "" ||
                tbNome.Text == "" ||
                tbCPostal.Text == "" ||
                tbNif.Text == ""
                )
            {
                MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
            }
            // Verificação de se algums campos estão preenchidos corretamente.
            else if (telCorreto + movCorreto + mailCorreto + cPostalCorreto != 4)
            {
                MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
            }
            else
            {
                // A utilização do procedimento para a criação de um registo na base de dados
                // com as informações didponibilizadas pelo utilizador e validadas anteriormente.
                using (GesSalEntities db = new GesSalEntities())
                {
                    List<clientes> qCliente;
                    qCliente = (from c in db.clientes
                                where (c.nif.ToString().Contains(tbNif.Text))
                                select c).ToList();

                    // Verificação de se existe algum contribuinte igual ao que foi introduzido
                    if (qCliente.Count > 0)
                    {
                        string aviso = "O cliente com o contribuinte " + tbNif.Text + " já existe.";
                        MessageBox.Show(aviso);
                    }
                    else
                    {
                        // Caso estaja tudo correto será gravado na Base de dados.
                        clientes novoCliente = new clientes()
                        {
                            nif = int.Parse(tbNif.Text),
                            nome = tbNome.Text,
                            morada = tbMorada.Text,
                            codigoPostal = tbCPostal.Text,
                            localidade = tbLocalidade.Text,
                            concelho = tbConcelho.Text,
                            distrito = tbDistrito.Text,
                            contacto = tbContacto.Text,
                            telefone = tbTelefone.Text,
                            telemovel = tbTelemovel.Text,
                            eMail = tbMail.Text,
                            site = tbSite.Text
                        };
                        db.clientes.Add(novoCliente);
                        db.SaveChanges();
                        Principal.Janela.AreaTrabalho.Content = new VerCliente();
                    }
                }
            }
        }


        // Verificação do preenchimento de alguns campos, se estão na configuração devida ou não, 
        // com a emissão do respetivo aviso. São utilizdas expressões regulares para o efeito
        // sendo o campo verificado logo após o preenchimento.

        /// <summary>
        /// Interação lógica para a ação de abandono de um dos campos Telefone,
        /// Telemóvel ou Nif.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis a 1 para o caso de ter sido colocada a 0.
            if (textoRec.Name == "tbTelefone")
            { telCorreto = 1; }
            else
            { movCorreto = 1; }
            Regex regex = new Regex("^[0-9]{9}$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter precisamente 9 dígitos de 0 a 9, sem espaços.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbTelefone")
                { telCorreto = textoRec.Text == "" ? 1 : 0; }
                else
                { movCorreto = textoRec.Text == "" ? 1 : 0; }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Mail.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbMail_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            mailCorreto = 1;
            Regex regex = new Regex("^[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+(?:.[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+)*@[A-Za-z0-9-]+(?:.[A-Za-z0-9-]+)*$");
            if (!regex.IsMatch(tbMail.Text))
            {
                aviso = tbMail.Text + " - Não parece ser um endereço de mail válido.";
                MessageBox.Show(aviso);
                mailCorreto = tbMail.Text == "" ? 1 : 0;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Codigo Postal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbCPostal_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            cPostalCorreto = 1;
            Regex regex = new Regex("^[0-9]{4}-[0-9]{3}$");
            if (!regex.IsMatch(tbCPostal.Text))
            {
                aviso = tbCPostal.Text + " - Não é um Código Postal válido, deve inserir no formato XXXX-XXX.";
                MessageBox.Show(aviso);
                cPostalCorreto = tbCPostal.Text == "" ? 1 : 0;
            }
        }
    }
}
