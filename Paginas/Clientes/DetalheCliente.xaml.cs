﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Clientes
{
    /// <summary>
    /// Interação lógica para DetalheCliente.xaml
    /// </summary>
    public partial class DetalheCliente : Page
    {
        // Criação de variável para guardar cliente selecionado.
        int selecao = VerCliente.JanCli.numOrd[VerCliente.JanCli.indiceAVista];

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public DetalheCliente()
        {
            // Inicialização dos componentes desta classe.
            InitializeComponent();

            // Pesquisa e preenchimento das labels e grids correspondentes.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var clienteSelecionado = (from c in sacaDados.clientes
                                          where c.nif == selecao
                                          select c).FirstOrDefault();
                gbNif.Content = clienteSelecionado.nif;
                gbNome.Content = clienteSelecionado.nome;

                var qAgenda = (from c in sacaDados.agenda
                               where c.nif == selecao
                               select c).ToList();
                dgDados.ItemsSource = qAgenda;

            }

        }


        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butOk_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerCliente();
        }
    }
}
