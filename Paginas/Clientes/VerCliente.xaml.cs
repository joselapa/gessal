﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Clientes
{
    /// <summary>
    /// Interação lógica para VerCliente.xaml
    /// </summary>
    public partial class VerCliente : Page
    {
        // Criação de uma variável estática do tipo desta classe para conseguir  
        // ter acesso às propriedades publicas desta classe noutras classes 
        // desta aplicação.
        public static VerCliente JanCli;

        // Implementação de uma lista para receber um conjunto de indices de clientes
        // com a finalidade de utilizar os botões "Seguinte" e "Anterior" para ser posicionado 
        // os dados do edifício mais conveniente da últma pesquisa.
        public List<int> numOrd = new List<int>();

        // Implementação de uma váriável para identificar o cliente que está atulamente à vista.
        public int indiceAVista = 0;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public VerCliente()
        {
            // Inicialização da variável atrás criada.
            JanCli = this;
            InitializeComponent();

            // Criação de uma primeira lista para colocar em funcionamento
            // os botões de "Seguinte" e "Anterior".
            CriaIndice("", "", "");

            // Preenchimento de primeiros valores ceum cliente nos campos à vista.
            PassaParaUI(numOrd[indiceAVista]);
        }

        /// <summary>
        /// Método a aplicar a query para a criação da priemira lista.
        /// </summary>
        /// <param name="contrib"></param>
        /// <param name="morad"></param>
        /// <param name="contac"></param>
        private void CriaIndice(string contrib, string morad, string contac)
        {
            using (GesSalEntities baseDados = new GesSalEntities())
            {
                numOrd = (from c in baseDados.clientes
                          where (c.nif.ToString().Contains(contrib) && c.morada.Contains(morad) && c.contacto.Contains(contac))
                          select c.nif).ToList();
            }
        }

        /// <summary>
        /// Método para o preenchimento de valores nos campos apresentados com uma lista 
        /// pretendida.
        /// </summary>
        /// <param name="x"></param>
        public void PassaParaUI(int x)
        {
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var ClienteSelecionado = (from c in sacaDados.clientes
                                          where c.nif >= x
                                          select c).FirstOrDefault();

                gbNif.Content = ClienteSelecionado.nif;
                gbNome.Content = ClienteSelecionado.nome;
                gbContacto.Content = ClienteSelecionado.contacto;
                gbTelefone.Content = ClienteSelecionado.telefone;
                gbTelemovel.Content = ClienteSelecionado.telemovel;
                gbMail.Content = ClienteSelecionado.eMail;
                gbMorada.Content = ClienteSelecionado.morada;
                gbCPostal.Content = ClienteSelecionado.codigoPostal;
                gbLocalidade.Content = ClienteSelecionado.localidade;
                gbConcelho.Content = ClienteSelecionado.concelho;
                gbDistrito.Content = ClienteSelecionado.distrito;
                gbSite.Content = ClienteSelecionado.site;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Pesquisar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butPesquisar_Click(object sender, RoutedEventArgs e)
        {
            PesqCliente JanPesq = new PesqCliente();
            JanPesq.ShowDialog();
        }


        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Detalhes".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butDetalhes_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new DetalheCliente();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Seguinte".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butSeguinte_Click(object sender, RoutedEventArgs e)
        {

            if (indiceAVista + 1 < numOrd.Count)
            {
                indiceAVista++;
                PassaParaUI(numOrd[indiceAVista]);
            }
        }


        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Anterior".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butAnterior_Click(object sender, RoutedEventArgs e)
        {
            if (indiceAVista - 1 >= 0)
            {
                indiceAVista--;
                PassaParaUI(numOrd[indiceAVista]);
            }
        }


        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Alterar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butAlterar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new AlteraCliente();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Criar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCriar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new CriarCliente();
        }
    }
}
