﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace GesSal.Paginas.Clientes
{
    /// <summary>
    /// Interação lógica para AlteraCliente.xaml
    /// </summary>
    public partial class AlteraCliente : Page
    {
        // Criação de variável para guardar cliente selecionado.
        int selecao = VerCliente.JanCli.numOrd[VerCliente.JanCli.indiceAVista];

        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int telCorreto, movCorreto, mailCorreto, cPostalCorreto = 1;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public AlteraCliente()
        {
            // Inicialização dos componentes desta classe.
            InitializeComponent();

            // Pesquisa e preenchimento das labels e campos de texto correspondentes.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var ClienteSelecionado = (from c in sacaDados.clientes
                                          where c.nif == selecao
                                          select c).FirstOrDefault();
                lbNif.Content = ClienteSelecionado.nif.ToString();
                tbNome.Text = ClienteSelecionado.nome;
                tbContacto.Text = ClienteSelecionado.contacto;
                tbTelefone.Text = ClienteSelecionado.telefone;
                tbTelemovel.Text = ClienteSelecionado.telemovel;
                tbMail.Text = ClienteSelecionado.eMail;
                tbMorada.Text = ClienteSelecionado.morada;
                tbCPostal.Text = ClienteSelecionado.codigoPostal;
                tbLocalidade.Text = ClienteSelecionado.localidade;
                tbConcelho.Text = ClienteSelecionado.concelho;
                tbDistrito.Text = ClienteSelecionado.distrito;
                tbSite.Text = ClienteSelecionado.site;
            }
        }

        // Verificação do preenchimento de alguns campos, se estão na configuração devida ou não, 
        // com a emissão do respetivo aviso. São utilizdas expressões regulares para o efeito
        // sendo o campo verificado logo após o preenchimento.

        /// <summary>
        /// Interação lógica para a ação de abandono de um dos campos Telefone e Telemóvel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis a 1 para o caso de ter sido colocada a 0.
            if (textoRec.Name == "tbTelefone")
            { telCorreto = 1; }
            else
            { movCorreto = 1; }
            Regex regex = new Regex("^[0-9]{9}$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter precisamente 9 dígitos de 0 a 9, sem espaços.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbTelefone")
                { telCorreto = textoRec.Text == "" ? 1 : 0; }
                else
                { movCorreto = textoRec.Text == "" ? 1 : 0; }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Mail.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbMail_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            mailCorreto = 1;
            Regex regex = new Regex("^[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+(?:.[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+)*@[A-Za-z0-9-]+(?:.[A-Za-z0-9-]+)*$");
            if (!regex.IsMatch(tbMail.Text))
            {
                aviso = tbMail.Text + " - Não parece ser um endereço de mail válido.";
                MessageBox.Show(aviso);
                mailCorreto = tbMail.Text == "" ? 1 : 0;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Código Postal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbCPostal_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            cPostalCorreto = 1;
            Regex regex = new Regex("^[0-9]{4}-[0-9]{3}$");
            if (!regex.IsMatch(tbCPostal.Text))
            {
                aviso = tbCPostal.Text + " - Não é um Código Postal válido, deve inserir no formato XXXX-XXX.";
                MessageBox.Show(aviso);
                cPostalCorreto = tbCPostal.Text == "" ? 1 : 0;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butOk_Click(object sender, RoutedEventArgs e)
        {

            // Verificação de se os campos obrigatórios estão preenchidos
            if (tbConcelho.Text == "" ||
                tbDistrito.Text == "" ||
                tbLocalidade.Text == "" ||
                tbMorada.Text == "" ||
                tbNome.Text == "" ||
                tbCPostal.Text == "")
            {
                MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
            }
            // Verificação de se algums campos estão preenchidos corretamente.
            else if (telCorreto + movCorreto + mailCorreto + cPostalCorreto != 4)
            {
                MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
            }
            else
            {
                using (GesSalEntities sacaDados = new GesSalEntities())
                {
                    clientes meuCliente = sacaDados.clientes.Where(x => x.nif == selecao).FirstOrDefault();

                    meuCliente.nome = tbNome.Text;
                    meuCliente.contacto = tbContacto.Text;
                    meuCliente.telefone = tbTelefone.Text;
                    meuCliente.telemovel = tbTelemovel.Text;
                    meuCliente.eMail = tbMail.Text;
                    meuCliente.morada = tbMorada.Text;
                    meuCliente.codigoPostal = tbCPostal.Text;
                    meuCliente.localidade = tbLocalidade.Text;
                    meuCliente.concelho = tbConcelho.Text;
                    meuCliente.distrito = tbDistrito.Text;
                    meuCliente.site = tbSite.Text;
                    sacaDados.SaveChanges();
                }
                Principal.Janela.AreaTrabalho.Content = new VerCliente();
            }

        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerCliente();
        }


        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Eliminar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Pretende mesmo eliminar este Cliente?", "A Eliminar...", MessageBoxButton.YesNo).ToString() == "Yes")
            {

                using (GesSalEntities db = new GesSalEntities())
                {
                    List<agenda> qAgenda;
                    qAgenda = (from c in db.agenda
                               where (c.nif.ToString().Contains(lbNif.Content.ToString()))
                               select c).ToList();

                    // Verificação de se existe algum evento que o cliente tenha organizado.
                    if (qAgenda.Count > 0)
                    {
                        string aviso = "O cliente com o contribuinte " + lbNif.Content + " não pode ser eliminado pois tem eventos atribuidos.";
                        MessageBox.Show(aviso);
                    }
                    else
                    {
                        // Se as confdições se verificarem o cliente é eliminado.
                        clientes removeCliente = db.clientes.Where(x => x.nif == selecao).FirstOrDefault();
                        db.clientes.Remove(removeCliente);
                        db.SaveChanges();
                    }
                }
            }
            Principal.Janela.AreaTrabalho.Content = new VerCliente();
        }
    }
}
