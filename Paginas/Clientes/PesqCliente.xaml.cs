﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Clientes
{
    /// <summary>
    /// Interação lógica para PesqCliente.xaml
    /// </summary>
    public partial class PesqCliente : Window
    {
        // Criação de variável que serve para guardar o cliente selecionado para passar 
        // no final ao user interface de Ver Cliente.
        int verSelecao = 0;

        // Criação de uma lista para preservar a pesquisa feita nesta janela.
        List<clientes> qCliente;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public PesqCliente()
        {
            InitializeComponent();

            // Aplicação da query à base de dados para iniciar a lista atrás criada
            // e apresenta-la no dataGrid correspondente.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                qCliente = (from c in sacaDados.clientes select c).ToList();
                dgDados.ItemsSource = qCliente;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de alteração de seleção no elemento da DataGrid
        /// atualizando a variável de verSeleção com o valor correspondente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgDados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;

            if (dataGrid.CurrentItem != null && e.AddedItems.Count > 0)
            {
                verSelecao = ((clientes)dataGrid.CurrentItem).nif;
            }
            else
            {
                verSelecao = 0;
            }

        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Selecionar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butSelecionar_Click(object sender, RoutedEventArgs e)
        {
            // Passagem dos valores da seleção feita para as variáveis do user interface correspondente.
            if (verSelecao != 0)
            {
                VerCliente.JanCli.numOrd = (from c in qCliente select c.nif).ToList();
                VerCliente.JanCli.indiceAVista = VerCliente.JanCli.numOrd.FindIndex(x => x == verSelecao);
                VerCliente.JanCli.PassaParaUI(VerCliente.JanCli.numOrd[VerCliente.JanCli.indiceAVista]);
                Close();
            }
            else
            {
                MessageBox.Show("Tem de primeiro selecionar um Cliente!");
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Interação lógica para a ação de escrever nos campos apresentados para criar a 
        /// lista de pesquisa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            // Instanciação DataGrid para apresentar a lista dos resultados apresentados.
            DataGrid dataGrid = sender as DataGrid;

            // Implementação da query onde se escreve o que se pretende pesquisar.
            using (GesSalEntities baseDados = new GesSalEntities())
            {
                qCliente = (from c in baseDados.clientes
                            where (c.nif.ToString().Contains(tbNif.Text) && c.nome.Contains(tbNome.Text) && c.contacto.Contains(tbContacto.Text))
                            select c).ToList();
                dgDados.ItemsSource = qCliente;
            }

        }
    }
}
