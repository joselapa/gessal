﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using DataGrid = System.Windows.Controls.DataGrid;
using MessageBox = System.Windows.MessageBox;
using TextBox = System.Windows.Controls.TextBox;

namespace GesSal.Paginas.Equipamentos
{
    /// <summary>
    /// Interação lógica para AlteraEquip.xaml
    /// </summary>
    public partial class AlteraEquip : Page
    {

        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int pesoCorreto, altCorreto, largCorreto, compCorreto, virgulaCorreta = 1;

        // Variável para criar a string com o caminho definitivo da pasta das imagens.
        string pastaImagens = @"\\" + Principal.Janela.DominioGesSala + @"\GesSala\Imagens\Equip\";

        // Variável para criar a string com o caminho e o nome de uma nova pasta temporária que corresponde ao do user ativo.
        string pathString = @"\\" + Principal.Janela.DominioGesSala + @"\GesSala\Imagens\Equip\" + Principal.Janela.UtilizadorGesSala;

        // Variável para a eliminação de imagens colocados por engano.
        string eliminaImagem = "";

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public AlteraEquip()
        {
            InitializeComponent();

            // Criação da variável para a receção do ID do equipamento seleciolnado.
            int.TryParse(VerEquip.JanelaVerEquip.verSelecao, out int selecao);

            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                // Variável para receber a lista para preencher a combobox.
                var qSala = (from c in sacaDados.salas select c).ToList();

                // Variável para receber os dados para preencher os restantes campos com os valores da base de dados. 
                var a = sacaDados.equip.Where(x => x.idEquip == selecao).FirstOrDefault();

                // Preenchimento dos campos de texto.
                tbIdentificacao.Text = a.nome;
                tbCaracteristicas.Text = a.caracteristicas;
                tbModelo.Text = a.modelo;
                tbCor.Text = a.cor;
                tbPeso.Text = a.peso.ToString();
                tbValor.Text = a.preco.ToString();
                tbAltura.Text = a.altura.ToString();
                tbComprimento.Text = a.comprimento.ToString();
                tbLargura.Text = a.largura.ToString();

                // Atribuição da lista á combobox.
                cbSala.ItemsSource = qSala;

                // Selecionar a combobox com o valor da base de dados.
                cbSala.SelectedIndex = qSala.FindIndex(x => x.idSala.Equals(a.idSala));

            }

            // Criação do diretório temporário para colocar as fotos alusivas ao equipamento selecionado,
            // alterando o nome do ficheiro para mellhor controlo.
            Directory.CreateDirectory(pathString);
            List<ListaImagens> listaImagens = new List<ListaImagens>();
            using (GesSalEntities sacaImagens = new GesSalEntities())
            {
                List<string> qEquip = (from c in sacaImagens.fotosEquip
                                       where c.idEquip.ToString() == VerEquip.JanelaVerEquip.verSelecao.ToString()
                                       select c.fotoPath).ToList();
                int i = 0;
                foreach (string registo in qEquip)
                {
                    i++;
                    byte[] recebe = null;
                    recebe = File.ReadAllBytes(registo);
                    listaImagens.Add(new ListaImagens() { Sitio = pathString + @"\" + i + ".jpg", Imagem = recebe });
                    File.Copy(registo, pathString + @"\" + i + ".jpg");
                }

                // Passagem da lista de imagens para o datgrid.
                dgSimple.ItemsSource = listaImagens;
            }
        }

        /// <summary>
        /// Classe criada com o objetivo de receber os arrays de bytes para serem mostradas no
        /// controlo imagem no front end, e deixar os ficheiro livre para se poder apagar.
        /// </summary>
        public class ListaImagens
        {
            public byte[] Imagem { get; set; }
            public string Sitio { get; set; }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Remover".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButRemover_Click(object sender, RoutedEventArgs e)
        {
            // Para a eliminação de uma imagem pressupõe-se uma seleção prévia, pelo que este
            // try-catch impõe essa condição e evita a apresentação de erro.
            try
            {
                // Eliminação da imagem, eliminado o ficheiro correspondente.
                File.Delete(eliminaImagem);

                // Criação de nova listagem de imagens com os ficheiros restantes na pasta temporária.
                string[] filePathList = Directory.GetFiles(pathString);
                List<ListaImagens> listaImagens = new List<ListaImagens>();
                foreach (var caminho in filePathList)
                {
                    byte[] recebe = null;
                    recebe = File.ReadAllBytes(caminho);
                    listaImagens.Add(new ListaImagens() { Sitio = caminho, Imagem = recebe });
                }

                // Passagem das imagens para a data grid.
                dgSimple.ItemsSource = listaImagens;
            }
            catch (Exception)
            {
                // Mensagem a passar em vez do erro.
                System.Windows.MessageBox.Show("Tem de selecionar primeiro uma imagem.");
            }

        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Adicionar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButAdicionar_Click(object sender, RoutedEventArgs e)
        {
            // Copiar o ficheiro colocado no campo do caminho, alterando ao mesmo tempo o nome
            // para se poder controlar o seu armazenamento.
            if (tbCaminho.Text == "" || !File.Exists(tbCaminho.Text))
            {
                System.Windows.MessageBox.Show("Ficheiro não especificado ou caminho não válido");
            }
            else
            {
                // Criação do nome para o ficheiro a colocar na pasta temporária.
                string fichDestino = "1.jpg";
                int i = 1;
                while (File.Exists(pathString + @"\" + fichDestino))
                {
                    i++;
                    fichDestino = i + ".jpg";
                }
                File.Copy(tbCaminho.Text, pathString + @"\" + fichDestino, true);

                // Criação da lista de imagens.
                string[] filePathList = Directory.GetFiles(pathString);
                List<ListaImagens> listaImagens = new List<ListaImagens>();
                foreach (var caminho in filePathList)
                {
                    byte[] recebe = null;
                    recebe = File.ReadAllBytes(caminho);
                    listaImagens.Add(new ListaImagens() { Sitio = caminho, Imagem = recebe });
                }
                // Passagem das imagens para a datagrid.
                dgSimple.ItemsSource = listaImagens;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            // Verificação e eliminação da pasta temporária, caso exista.
            if (Directory.Exists(pathString))
            {
                DirectoryInfo di = new DirectoryInfo(pathString);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                Directory.Delete(pathString);
            }
            // Passagem para o quadro inicial.
            Principal.Janela.AreaTrabalho.Content = new VerEquip();
        }



        private void ButOk_Click(object sender, RoutedEventArgs e)
        {

            // Verificação de se os campos obrigatórios estão preenchidos
            if (tbIdentificacao.Text == "" ||
                cbSala.SelectedIndex < 1
                )
            {
                MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
            }
            // Verificação de se algums campos estão preenchidos corretamente.
            else if (pesoCorreto + altCorreto + largCorreto+ compCorreto + virgulaCorreta != 5)
            {
                MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
            }
            else
            {
                // Para que se possa colocar os novos ficheiros eliminam-se primeiro os ficheiros originais.
                string[] picList = Directory.GetFiles(pastaImagens, VerEquip.JanelaVerEquip.verSelecao.ToString() + "_*");
                foreach (string ficheiro in picList)
                {
                    File.Delete(ficheiro);
                }

                using (GesSalEntities db = new GesSalEntities())
                {

                    int idEquip;

                    // Gravação dos dados na base de dados dos equipamentos.
                    if (int.TryParse(VerEquip.JanelaVerEquip.verSelecao, out idEquip))
                    {
                        equip meuEquip = db.equip.Where(x => x.idEquip == idEquip).FirstOrDefault();
                        meuEquip.nome = tbIdentificacao.Text;
                        meuEquip.modelo = tbModelo.Text;
                        meuEquip.comprimento = int.Parse(tbComprimento.Text);
                        meuEquip.largura = int.Parse(tbLargura.Text);
                        meuEquip.altura = int.Parse(tbAltura.Text);
                        meuEquip.cor = tbCor.Text;
                        meuEquip.peso = int.Parse(tbPeso.Text);
                        meuEquip.preco = float.Parse(tbValor.Text);
                        meuEquip.caracteristicas = tbCaracteristicas.Text;
                        db.SaveChanges();
                    }

                    int r = 0;

                    // Guarda-se de seguida os ficheiros da pasta temporária para a pasta definitiva com nome definitivo.
                    if (Directory.Exists(pathString))
                    {
                        foreach (var fiche in Directory.GetFiles(pathString))
                        {
                            r++;
                            string fichedestino = @"\\" + Principal.Janela.DominioGesSala + @"\GesSala\Imagens\Equip\" + VerEquip.JanelaVerEquip.verSelecao.ToString() + "_" + r + ".jpg";
                            File.Copy(fiche, fichedestino, true);

                        }

                        // Eliminação da pasta temporária
                        DirectoryInfo di = new DirectoryInfo(pathString);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        Directory.Delete(pathString);
                    }


                    var imageEquip = (from c in db.fotosEquip
                                      where c.idEquip.ToString() == VerEquip.JanelaVerEquip.verSelecao.ToString()
                                      select c).ToList();

                    picList = Directory.GetFiles(pastaImagens, VerEquip.JanelaVerEquip.verSelecao.ToString() + "_*");

                    // Guardar os caminhos dos novos ficheiro na tabela respetiva.
                    // Caso os caminhos já existam os mesmos preservam-se.
                    if (imageEquip.Count >= picList.Length)
                    {
                        foreach (var bdlist in imageEquip)
                        {
                            if (!File.Exists(bdlist.fotoPath.ToString()))
                            {
                                fotosEquip registo = db.fotosEquip.Where(x => x.idFotoEquip == bdlist.idFotoEquip).FirstOrDefault();
                                db.fotosEquip.Remove(registo);
                                db.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        foreach (var fiche in picList)
                        {
                            var novoRegisto = (from a in db.fotosEquip where a.fotoPath.Contains(fiche) select a).ToList();
                            if (novoRegisto.Count == 0)
                            {
                                fotosEquip novaFoto = new fotosEquip()
                                {
                                    fotoPath = fiche.ToString(),
                                    idEquip = int.Parse(VerEquip.JanelaVerEquip.verSelecao)
                                };
                                db.fotosEquip.Add(novaFoto);
                                db.SaveChanges();
                            }
                        }

                    }
                }
            }

            Principal.Janela.AreaTrabalho.Content = new VerEquip();
        }



        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Procurar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butProcurar_Click(object sender, RoutedEventArgs e)
        {
            // Abertura da janela de diálogo para se poder selecionar o ficheiro 
            // pretendido, já no formato ".JPG" e colocar o caminho no campo correspondente.
            // Caso o utilizador pretenda pode escrever o camhinho sem recorre a esta função.
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Ficheiros de imagens(*.jpeg)|*.jpg";
            openFileDialog.ShowDialog();
            tbCaminho.Text = openFileDialog.FileName;
        }

        /// <summary>
        /// Interação lógica para a ação alteração da seleção de uma imagem no correspondente datagrid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgSimple1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;

            // Este try catch é para evitar a exceção devido à eliminação do ficheiro
            // na pasta enquanto renova a viasualização das imagens, caso venha a ser
            // refrenciado um elemeto já eliminado.
            try
            {
                eliminaImagem = ((ListaImagens)dataGrid.CurrentItem).Sitio.ToString();
            }
            catch
            {

            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Eliminar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butEliminar_Click(object sender, RoutedEventArgs e)
        {

            if (MessageBox.Show("Pretende mesmo eliminar este equipamento?", "A Eliminar...", MessageBoxButton.YesNo).ToString() == "Yes")
            {
                // Na eliminação do equipameento é necessário eliminar também os ficheiros das imagens.
                using (GesSalEntities db = new GesSalEntities())
                {
                    // Criação da lista de imagens a eliminar.
                    var imagens = (from c in db.fotosEquip
                                   where c.idEquip.ToString() == VerEquip.JanelaVerEquip.verSelecao.ToString()
                                   select c).ToList();

                    // Eliminação de cada um dos ficheiros correspondentes ao equipamento a eliminar
                    // no dirétório definitivo.
                    foreach (var imagem in imagens)
                    {
                        File.Delete(imagem.fotoPath.ToString());
                        fotosEquip registo = db.fotosEquip.Where(x => x.idFotoEquip == imagem.idFotoEquip).FirstOrDefault();
                        db.fotosEquip.Remove(registo);
                        db.SaveChanges();
                    }

                    // Após os ficheiros eliminados elimina-se o registo na base de dados.
                    int.TryParse(VerEquip.JanelaVerEquip.verSelecao, out int selecao);
                    equip aEliminar = db.equip.Where(x => x.idEquip == selecao).FirstOrDefault();
                    db.equip.Remove(aEliminar);
                    db.SaveChanges();
                }
            }

            // Eliminação da pasta temporária que foi criada com o conceito de alterar as imagens.
            if (Directory.Exists(pathString))
            {
                DirectoryInfo di = new DirectoryInfo(pathString);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                Directory.Delete(pathString);
            }
            Principal.Janela.AreaTrabalho.Content = new VerEquip();
        }

        // Verificação do preenchimento de alguns campos, se estão na configuração devida ou não, 
        // com a emissão do respetivo aviso. São utilizdas expressões regulares para o efeito
        // sendo o campo verificado logo após o preenchimento.

        /// <summary>
        /// Interação lógica para a ação de abandono de um dos campos Comprimento,
        /// Altura, Largura ou Peso.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis a 1 para o caso de ter sido colocadas a 0.
            if (textoRec.Name == "tbAltura")
            { altCorreto = 1; }
            else if (textoRec.Name == "tbComprimento")
            { compCorreto = 1; }
            else if (textoRec.Name == "tbLargura")
            { largCorreto = 1; }
            else
            { pesoCorreto = 1; }
            Regex regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter apenas digitos.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbAltura")
                { altCorreto = textoRec.Text == "" ? 1 : 0; }
                else if (textoRec.Name == "tbComprimento")
                { compCorreto = textoRec.Text == "" ? 1 : 0; }
                else if (textoRec.Name == "tbLargura")
                { largCorreto = textoRec.Text == "" ? 1 : 0; }
                else
                { pesoCorreto = textoRec.Text == "" ? 1 : 0; }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campos Valor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbValor_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            virgulaCorreta = 1;
            TextBox textoRec = sender as TextBox;
            Regex regex = new Regex("^\\d*,?\\d*$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter apenas digitos ou , como separador de decimais.";
                MessageBox.Show(aviso);
                virgulaCorreta = textoRec.Text == "" ? 1 : 0;
            }
        }
    }
}
