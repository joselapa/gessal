﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace GesSal.Paginas.Equipamentos
{
    /// <summary>
    /// Interação lógica para VerImagens.xaml
    /// </summary>
    public partial class VerImagens : Window
    {

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public VerImagens()
        {
            InitializeComponent();

            // Variável para receber a informação do equipamento selecionado.
            string posicao =  VerEquip.JanelaVerEquip.verSelecao;

            List<ListaImagens> listaImagens = new List<ListaImagens>();

            using (GesSalEntities sacaImagens = new GesSalEntities())
            {

                // Pesquisa do caminho dos ficheiros da base de dados.
                List<string> qEquip = (from c in sacaImagens.fotosEquip
                             where c.idEquip.ToString() == posicao
                             select c.fotoPath).ToList();

                // Passagem dos bytesdas imagens dos caminhos encontrados atrás.
                foreach (string registo in qEquip)
                {
                    byte[] recebe = null;
                    recebe = File.ReadAllBytes(registo);
                    listaImagens.Add(new ListaImagens() {Imagem = recebe });
                }

                // Passagem dos dados para a datagrid
                dgImage.ItemsSource = listaImagens;
            }
        }


        /// <summary>
        /// Classe criada com o objetivo de receber os arrays de bytes para serem mostradas no
        /// controlo imagem no front end, e deixar os ficheiro livre para se poder apagar.
        /// </summary>
        public class ListaImagens
        {
            public byte[] Imagem { get; set; }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Fechar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fechar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
