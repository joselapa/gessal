﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Windows.Forms;
using DataGrid = System.Windows.Controls.DataGrid;
using MessageBox = System.Windows.MessageBox;
using System.Text.RegularExpressions;
using TextBox = System.Windows.Controls.TextBox;

namespace GesSal.Paginas.Equipamentos
{
    /// <summary>
    /// Interação lógica para CriarEquip.xaml
    /// </summary>
    public partial class CriarEquip : Page
    {
        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int pesoCorreto, altCorreto, largCorreto, compCorreto, virgulaCorreta = 1;

        // Variável para criar a string com o caminho e o nome de uma nova pasta temporária que corresponde ao do user ativo.
        string pathString = @"\\" + Principal.Janela.DominioGesSala + @"\GesSala\Imagens\Equip\" + Principal.Janela.UtilizadorGesSala;

        /// Variável para a eliminação de imagens colocadas na pasta temporária e que se pretendem retitrar.
        string eliminaImagem = "";

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public CriarEquip()
        {
            InitializeComponent();

            // Criação de uma query para o preenchimento da combobox da localização para a 
            // inserssão no campo correspondente.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var qSala = (from c in sacaDados.salas select c).ToList();
                cbSala.ItemsSource = qSala;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButCancelar_Click(object sender, RoutedEventArgs e)
        {
            // Verificação e eliminação da pasta temporária, caso exista, no caso do utilizador ter 
            // junto algumas fotos no entretanto.
            if (Directory.Exists(pathString))
            {
                DirectoryInfo di = new DirectoryInfo(pathString);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                Directory.Delete(pathString);
            }
            // Passagem para o quadro inicial.
            Principal.Janela.AreaTrabalho.Content = new VerEquip();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButOk_Click(object sender, RoutedEventArgs e)
        {

            // Verificação de se os campos obrigatórios estão preenchidos
            if (tbIdentificacao.Text == "" ||
                cbSala.SelectedIndex < 1
                )
            {
                MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
            }
            // Verificação de se algums campos estão preenchidos corretamente.
            else if (pesoCorreto + altCorreto + largCorreto + compCorreto + virgulaCorreta != 5)
            {
                MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
            }
            else
            {

                // A utilização do procedimento para a criação de um registo na base de dados
                // com as informações didponibilizadas pelo utilizador e validadas anteriormente.
                using (GesSalEntities db = new GesSalEntities())
                {
                    equip meuEquip = new equip()
                    {
                        nome = tbIdentificacao.Text,
                        modelo = tbModelo.Text,
                        comprimento = int.Parse(tbComprimento.Text),
                        largura = int.Parse(tbLargura.Text),
                        altura = int.Parse(tbAltura.Text),
                        cor = tbCor.Text,
                        peso = int.Parse(tbPeso.Text),
                        preco = float.Parse(tbValor.Text),
                        caracteristicas = tbCaracteristicas.Text,
                        idSala = ((salas)cbSala.SelectedValue).idSala
                    };
                    db.equip.Add(meuEquip);
                    db.SaveChanges();

                    // Criação de variáveis para o nome definitivo dos ficheiros das imagens.
                    int item = db.equip.OrderByDescending(i => i.idEquip).FirstOrDefault().idEquip;
                    int r = 0;

                    if (Directory.Exists(pathString))
                    {
                        // Cópia de cada um dos ficheiros para a pasta definitiva com o nome final.
                        foreach (var fiche in Directory.GetFiles(pathString))
                        {
                            r++;
                            string fichedestino = @"\\" + Principal.Janela.DominioGesSala + @"\GesSala\Imagens\Equip\" + item + "_" + r + ".jpg";
                            File.Copy(fiche, fichedestino, true);

                            // A utilização do procedimento para a criação de um registo na base de dados.
                            using (GesSalEntities db1 = new GesSalEntities())
                            {
                                fotosEquip fotografia = new fotosEquip()
                                {
                                    idEquip = item,
                                    fotoPath = fichedestino,
                                };
                                db1.fotosEquip.Add(fotografia);
                                db1.SaveChanges();
                            }
                        }

                        // Após copiar os ficheiros para a pasta definitiva é eliminiada a pasta temporária.
                        DirectoryInfo di = new DirectoryInfo(pathString);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        Directory.Delete(pathString);

                        // Passagem para o user interface da ver equipamento.
                        Principal.Janela.AreaTrabalho.Content = new VerEquip();
                    }
                }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Procurar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButProcurar_Click(object sender, RoutedEventArgs e)
        {
            // Abertura da janela de diálogo para se poder selecionar o ficheiro 
            // pretendido, já no formato ".JPG" e colocar o caminho no campo correspondente.
            // Caso o utilizador pretenda pode escrever o camhinho sem recorre a esta função.
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Ficheiros de imagens(*.jpeg)|*.jpg";
            openFileDialog.ShowDialog();
            tbCaminho.Text = openFileDialog.FileName;
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Remover".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButRemover_Click(object sender, RoutedEventArgs e)
        {
            // Para a eliminação de uma imagem pressupõe-se uma seleção prévia, pelo que este
            // try-catch impõe essa condição e evita a apresentação de erro.
            try
            {
                // Eliminação da imagem, eliminado o ficheiro correspondente.
                File.Delete(eliminaImagem);

                // Criação de nova listagem de imagens com os ficheiros restantes na pasta temporária.
                string[] filePathList = Directory.GetFiles(pathString);
                List<ListaImagens> listaImagens = new List<ListaImagens>();
                foreach (var caminho in filePathList)
                {
                    byte[] recebe = null;
                    recebe = File.ReadAllBytes(caminho);
                    listaImagens.Add(new ListaImagens() { Sitio = caminho, Imagem = recebe });
                }
                // Passagem das imagens para a data grid.
                dgSimple.ItemsSource = listaImagens;
            }
            catch (Exception)
            {
                // Mensagem a passar em vez do erro.
                System.Windows.MessageBox.Show("Tem de selecionar primeiro uma imagem.");
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Adicionar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButAdicionar_Click(object sender, RoutedEventArgs e)
        {

            // instrução para criar uma nova pasta temporária caso ainda não exista.
            if (!Directory.Exists(pathString))
                Directory.CreateDirectory(pathString);


            // Copiar o ficheiro colocado no campo do caminho, alterando ao mesmo tempo o nome
            // para se poder controlar o seu armazenamento.
            if (tbCaminho.Text == "" || !File.Exists(tbCaminho.Text))
            {
                System.Windows.MessageBox.Show("Ficheiro não especificado ou caminho não válido");
            }
            else
            {
                // Criação do nome para o ficheiro a colocar na pasta temporária.
                string fichDestino = "1.jpg";
                int i = 1;
                while (File.Exists(pathString + @"\" + fichDestino))
                {
                    i++;
                    fichDestino = i + ".jpg";
                }
                File.Copy(tbCaminho.Text, pathString + @"\" + fichDestino, true);

                // Criação da lista de imagens.
                string[] filePathList = Directory.GetFiles(pathString);
                List<ListaImagens> listaImagens = new List<ListaImagens>();
                foreach (var caminho in filePathList)
                {
                    byte[] recebe = null;
                    recebe = File.ReadAllBytes(caminho);
                    listaImagens.Add(new ListaImagens() { Sitio = caminho, Imagem = recebe });
                }
                // Passagem das imagens para a datagrid.
                dgSimple.ItemsSource = listaImagens;
            }
        }

        /// <summary>
        /// Classe criada com o objetivo de receber os arrays de bytes para serem mostradas no
        /// controlo imagem no front end, e deixar os ficheiro livre para se poder apagar.
        /// </summary>
        public class ListaImagens
        {
            public string Sitio { get; set; }
            public byte[] Imagem { get; set; }
        }

        /// <summary>
        ///  Interação lógica para a ação alteração da seleção de uma imagem no correspondente datagrid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgSimple_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;

            // Este try catch é para evitar a exceção devido à eliminação do ficheiro
            // na pasta enquanto renova a viasualização das imagens, caso venha a ser
            // refrenciado um elemeto já eliminado.
            try
            {
                eliminaImagem = ((ListaImagens)dataGrid.CurrentItem).Sitio.ToString();
            }
            catch
            {

            }
        }
        
        // Verificação do preenchimento de alguns campos, se estão na configuração devida ou não, 
        // com a emissão do respetivo aviso. São utilizdas expressões regulares para o efeito
        // sendo o campo verificado logo após o preenchimento.

        /// <summary>
        /// Interação lógica para a ação de abandono de um dos campos Comprimento,
        /// Altura, Largura ou Peso.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis a 1 para o caso de ter sido colocadas a 0.
            if (textoRec.Name == "tbAltura")
            { altCorreto = 1; }
            else if (textoRec.Name == "tbComprimento")
            { compCorreto = 1; }
            else if (textoRec.Name == "tbLargura")
            { largCorreto = 1; }
            else
            { pesoCorreto = 1; }
            Regex regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter apenas digitos.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbAltura")
                { altCorreto = textoRec.Text == "" ? 1 : 0; }
                else if (textoRec.Name == "tbComprimento")
                { compCorreto = textoRec.Text == "" ? 1 : 0; }
                else if (textoRec.Name == "tbLargura")
                { largCorreto = textoRec.Text == "" ? 1 : 0; }
                else
                { pesoCorreto = textoRec.Text == "" ? 1 : 0; }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campos Valor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbValor_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            virgulaCorreta = 1;
            TextBox textoRec = sender as TextBox;
            Regex regex = new Regex("^\\d*,?\\d*$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter apenas digitos ou , como separador de decimais.";
                MessageBox.Show(aviso);
                virgulaCorreta = textoRec.Text == "" ? 1 : 0;
            }
        }
    }
}
