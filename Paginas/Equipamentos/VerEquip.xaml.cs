﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Equipamentos
{
    /// <summary>
    /// Interação lógica para VerEquip.xaml
    /// </summary>
    public partial class VerEquip : Page
    {

        // Implementação de uma váriável para identificar a seleção feita pelo utilizador.
        public string verSelecao = "0";

        // Criação de uma variável estática do tipo desta classe para conseguir  
        // ter acesso às propriedades publicas desta classe noutras classes 
        // desta aplicação.
        public static VerEquip JanelaVerEquip;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public VerEquip()
        {
            InitializeComponent();

            //Inicialização da variável estática criada atrás.
            JanelaVerEquip = this;

            using (GesSalEntities sacaDados = new GesSalEntities())
            {

                //Criação de uma variável para receber os dados da query feita.
                var qEquip = (from c in sacaDados.equip select c).ToList();

                // Passagem dos dados para a datagrid.
                dgDados.ItemsSource = qEquip;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Imagens".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butImagens_Click(object sender, RoutedEventArgs e)
        {
            Paginas.Equipamentos.VerImagens JanVerImg = new Paginas.Equipamentos.VerImagens();
            JanVerImg.ShowDialog();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Criar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCriar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new CriarEquip();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Alterar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butAlterar_Click(object sender, RoutedEventArgs e)
        {
            // Verificação da existência de uma seleção da tabela dos equipamentos.
            if (verSelecao == "0")
            {
                MessageBox.Show("Tem de selcionar primeiro um equipamento.");
            }
            else
            {
                Principal.Janela.AreaTrabalho.Content = new AlteraEquip();
            }

        }

        /// <summary>
        /// Interação lógica para a ação de selecionar um rigisto da datagrid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgDados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;
            verSelecao = ((equip)dataGrid.CurrentItem).idEquip.ToString();
        }
    }
}
