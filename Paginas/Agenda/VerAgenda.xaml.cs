﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Agenda
{
    /// <summary>
    /// Interação lógica para VerAgenda.xaml
    /// </summary>
    public partial class VerAgenda : Page
    {
        // Implementação de uma váriável para identificar a seleção feita pelo utilizador.
        public string verSelecao = "0";

        // Criação de uma variável estática do tipo desta classe para conseguir  
        // ter acesso às propriedades publicas desta classe noutras classes 
        // desta aplicação.
        public static VerAgenda JanelaVerAgenda;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public VerAgenda()
        {
            //Inicialização da variável estática criada atrás.
            JanelaVerAgenda = this;
            InitializeComponent();

            using (GesSalEntities sacaDados = new GesSalEntities())
            {

                //Criação de uma variável para receber os dados da query feita.
                var qAgenda = (from c in sacaDados.agenda
                               join b in sacaDados.clientes
                               on c.nif equals b.nif
                               join ty in sacaDados.salas
                               on c.idSala equals ty.idSala
                               select new {c.idAgenda, c.evento, c.dataIn, c.dataOut, c.horaIn, c.horaOut,
                               c.responsavel, c.telemovel, c.observacoes, b.nome, ty.identificacao}).ToList();

                // Passagem dos dados para a datagrid.
                dgDados.ItemsSource = qAgenda;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Criar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCriar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new CriarAgenda();
        }
        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Alterar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butAlterar_Click(object sender, RoutedEventArgs e)
        {
            // Verificação da existência de uma seleção da tabela das salas.
            if (verSelecao == "0")
            {
                MessageBox.Show("Tem de selcionar primeiro um evento.");
            }
            else
            {
                Principal.Janela.AreaTrabalho.Content = new AlterarAgenda();
            }
        }

        /// <summary>
        /// Interação lógica para a ação de selecionar um rigisto da datagrid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgDados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var type = dgDados.CurrentItem.GetType();

            if (dgDados.CurrentItem != null && e.AddedItems.Count > 0)
            {
                verSelecao = ((int)type.GetProperty("idAgenda").GetValue(dgDados.CurrentItem, null)).ToString();
            }
            else
            {
                verSelecao = "0";
            }

        }
    }
}
