﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace GesSal.Paginas.Agenda
{
    /// Interação lógica para CriarAgenda.xaml
    public partial class CriarAgenda : Page
    {
        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int numCorreto, minInCorreto, minOutCorreto, horaInCorreto, horaOutCorreto, mailCorreto = 1;

        /// Implementação do método construtor desta classe.
        public CriarAgenda()
        {
            InitializeComponent();
            // Preenchimento das combobox's existentes.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var qCliente = (from b in sacaDados.clientes select b).ToList();
                var qSala = (from c in sacaDados.salas select c).ToList();
                cbSala.ItemsSource = qSala;
                cbCliente.ItemsSource = qCliente;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerAgenda();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butOk_Click(object sender, RoutedEventArgs e)
        {
            // Convrsão dos valores introduzidos nos campos de horas em variáveis de tempo.
            TimeSpan horasInicio = new TimeSpan(int.Parse(tbHoraIn.Text), int.Parse(tbMinutoIn.Text), 0);
            TimeSpan horasFim = new TimeSpan(int.Parse(tbHoraOut.Text), int.Parse(tbMinutoOut.Text), 0);

            // Salvaguarda dos dados na base de dados.
            using (GesSalEntities db = new GesSalEntities())
            {

                // Verificação de se os campos obrigatórios estão preenchidos
                if (tbEvento.Text == "" ||
                    tbResposavel.Text == "" ||
                    tbTelemovel.Text == "" ||
                    tbMail.Text == "" ||
                    tbHoraIn.Text == "" ||
                    tbHoraOut.Text == "" ||
                    tbMinutoIn.Text == "" ||
                    tbMinutoOut.Text == "" ||
                   dpDataIn.Text == "" ||
                   dpDataOut.Text == "" ||
                   cbCliente.SelectedIndex < 1 ||
                   cbSala.SelectedIndex < 1
                    )
                {
                    MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
                }

                // Verificação de se algums campos estão preenchidos corretamente.
                else if (numCorreto + mailCorreto + minInCorreto + minOutCorreto + horaInCorreto + horaOutCorreto < 6)
                {
                    MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
                }

                // Verificação de se as horas estão preenchidas corretamente.
                else if (dpDataIn.SelectedDate == dpDataOut.SelectedDate && horasInicio >= horasFim)
                {
                    MessageBox.Show("A hora de finalização do evento deve ser sempre superior à de início");
                }
                else
                {
                    // Passagem dos dados colocados nos campos de testo na base de dadso.
                    agenda novaAgenda = new agenda()
                    {
                        evento = tbEvento.Text,
                        nif = ((clientes)cbCliente.SelectedValue).nif,
                        idSala = ((salas)cbSala.SelectedValue).idSala,
                        dataIn = DateTime.Parse(dpDataIn.Text),
                        dataOut = DateTime.Parse(dpDataOut.Text),
                        horaIn = horasInicio,
                        horaOut = horasFim,
                        responsavel = tbResposavel.Text,
                        telemovel = tbTelemovel.Text,
                        observacoes = tbObservacoes.Text,
                        eMail = tbMail.Text
                    };
                    db.agenda.Add(novaAgenda);
                    db.SaveChanges();
                    Principal.Janela.AreaTrabalho.Content = new VerAgenda();
                }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de alteração da data de início.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dpDataIn_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpDataIn.SelectedDate < DateTime.Today)
            {
                MessageBox.Show("A data de início não pode inferior à data de hoje");
            }
        }

        /// <summary>
        /// Interação lógica para a ação de alteração da data de fim.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dpDataOut_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpDataOut.SelectedDate < dpDataIn.SelectedDate)
            {
                MessageBox.Show("A data de fim não pode inferior à data de início");
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Mail.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbMail_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            mailCorreto = 1;
            Regex regex = new Regex("^[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+(?:.[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+)*@[A-Za-z0-9-]+(?:.[A-Za-z0-9-]+)*$");
            if (!regex.IsMatch(tbMail.Text))
            {
                aviso = tbMail.Text + " - Não parece ser um endereço de mail válido.";
                MessageBox.Show(aviso);
                mailCorreto = 0;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Telefone.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            numCorreto = 1;
            TextBox textoRec = sender as TextBox;
            Regex regex = new Regex("^[0-9]{9}$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter precisamente 9 dígitos de 0 a 9, sem espaços.";
                MessageBox.Show(aviso);
                numCorreto = 0;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono de qualquer um dos campos de minutos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void min_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis correspondentes a 1 para o caso de ter sido colocada a 0.
            if (textoRec.Name == "tbMinutoIn")
            { minInCorreto = 1; }
            else
            { minOutCorreto = 1; }

            Regex regex = new Regex("^[0-5][0-9]$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - O valor para minutos deve estar entre 00 e 59.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbMinutoIn")
                { minInCorreto = 0; }
                else
                { minOutCorreto = 0; }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono de qualquer um dos campos de hora.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void hora_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis correspondentes a 1 para o caso de ter sido colocada a 0.
            if (textoRec.Name == "tbHoraIn")
            { horaInCorreto = 1; }
            else
            { horaOutCorreto = 1; }
            Regex regex = new Regex("^[0-1][0-9]|2[0-3]$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - O valor para horas deve estar entre 00 e 23.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbHoraIn")
                { horaInCorreto = 0; }
                else
                { horaOutCorreto = 0; }
            }
        }

    }
}
