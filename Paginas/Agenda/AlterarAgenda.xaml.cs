﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace GesSal.Paginas.Agenda
{
    /// Interação lógica para AlterarAgenda.xaml
    public partial class AlterarAgenda : Page
    {
        // Criação de variável para guardar cliente selecionado.
        int selecao = int.Parse(VerAgenda.JanelaVerAgenda.verSelecao);

        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int numCorreto, minInCorreto, minOutCorreto, horaInCorreto, horaOutCorreto, mailCorreto = 1;

        /// Implementação do método construtor desta classe.
        public AlterarAgenda()
        {
            InitializeComponent();
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                // Atribuição das listas á combobox's respectivas.
                var qCliente = (from b in sacaDados.clientes select b).ToList();
                cbCliente.ItemsSource = qCliente;
                var qSala = (from c in sacaDados.salas select c).ToList();
                cbSala.ItemsSource = qSala;

                // Preenchimento dos campos de texto.
                var a = sacaDados.agenda.Where(x => x.idAgenda == selecao).FirstOrDefault();
                tbEvento.Text = a.evento.ToString();
                tbResposavel.Text = a.responsavel;
                tbTelemovel.Text = a.telemovel;
                tbObservacoes.Text = a.observacoes;
                tbMail.Text = a.eMail;
                tbHoraIn.Text = a.horaIn.Hours.ToString();
                tbMinutoIn.Text = a.horaIn.Minutes.ToString();
                tbHoraOut.Text = a.horaOut.Hours.ToString();
                tbMinutoOut.Text = a.horaOut.Minutes.ToString();

                // Configurar as combobox's com o valores da base de dados correspondentes.
                cbSala.SelectedIndex = qSala.FindIndex(s => s.idSala.Equals(a.idSala));
                cbCliente.SelectedIndex = qCliente.FindIndex(s => s.nif.Equals(a.nif));

                // Configurar os datepicker's com o valores da base de dados correspondentes.
                dpDataIn.SelectedDate = a.dataIn;
                dpDataOut.SelectedDate = a.dataOut;

                // Após o preenchimento de todos os campos, alguns podem ter alterado o estado inicial, pelo que
                // é necessário reiniciar todas as variáveis.
                numCorreto = minInCorreto = minOutCorreto = horaInCorreto = horaOutCorreto = mailCorreto =1;
            }
        }

        /// Interação lógica para a ação de clicar no botão "Cancelar".
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerAgenda();
        }
        /// Interação lógica para a ação de clicar no botão "Ok".
        private void butOk_Click(object sender, RoutedEventArgs e)
        {
            // Inicialização das variáveis de horas.
            TimeSpan horasInicio = new TimeSpan(int.Parse(tbHoraIn.Text), int.Parse(tbMinutoIn.Text), 0);
            TimeSpan horasFim = new TimeSpan(int.Parse(tbHoraOut.Text), int.Parse(tbMinutoOut.Text), 0);

            // Verificação de se os campos obrigatórios estão preenchidos
            if (tbEvento.Text == "" || tbResposavel.Text == "" || tbTelemovel.Text == "" || tbMail.Text == "" ||
                tbHoraIn.Text == "" || tbHoraOut.Text == "" || tbMinutoIn.Text == "" || tbMinutoOut.Text == "" ||
                dpDataIn.Text == "" || dpDataOut.Text == "" || cbCliente.SelectedIndex < 0 || cbSala.SelectedIndex < 0)
            {
                MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
            }
            // Verificação de se algums campos estão preenchidos corretamente.
            else if (numCorreto + mailCorreto + minInCorreto + minOutCorreto + horaInCorreto + horaOutCorreto < 6)
            {
                MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
            }
            // Verificação de se as horas estão preenchidas corretamente.
            else if (dpDataIn.SelectedDate == dpDataOut.SelectedDate && horasInicio >= horasFim)
            {
                MessageBox.Show("A hora de finalização do evento deve ser sempre superior à de início");
            }
            else
            {
                // Passagem dos dados colocados nos campos de testo na base de dadso.
                using (GesSalEntities db = new GesSalEntities())
                {
                    agenda minhaAgenda = db.agenda.Where(x => x.idAgenda == selecao).FirstOrDefault();
                    minhaAgenda.evento = tbEvento.Text;
                    minhaAgenda.nif = ((clientes)cbCliente.SelectedValue).nif;
                    minhaAgenda.idSala = ((salas)cbSala.SelectedValue).idSala;
                    minhaAgenda.dataIn = DateTime.Parse(dpDataIn.Text);
                    minhaAgenda.dataOut = DateTime.Parse(dpDataOut.Text);
                    minhaAgenda.horaIn = horasInicio;
                    minhaAgenda.horaOut = horasFim;
                    minhaAgenda.responsavel = tbResposavel.Text;
                    minhaAgenda.telemovel = tbTelemovel.Text;
                    minhaAgenda.observacoes = tbObservacoes.Text;
                    minhaAgenda.eMail = tbMail.Text;
                    db.SaveChanges();
                }
                Principal.Janela.AreaTrabalho.Content = new VerAgenda();
            }
        }
        /// Interação lógica para a ação de clicar no botão "Eliminar".
        private void butEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Pretende mesmo eliminar esta marcação?", "A Eliminar...", MessageBoxButton.YesNo).ToString() == "Yes")
            {
                using (GesSalEntities db = new GesSalEntities())
                {
                    int.TryParse(VerAgenda.JanelaVerAgenda.verSelecao, out int selecao);
                    agenda aEliminar = db.agenda.Where(x => x.idAgenda == selecao).FirstOrDefault();
                    db.agenda.Remove(aEliminar);
                    db.SaveChanges();
                }
                Principal.Janela.AreaTrabalho.Content = new VerAgenda();
            }
        }
        /// Interação lógica para a ação de alteração da data de fim.
        private void dpDataOut_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dpDataOut.SelectedDate < dpDataIn.SelectedDate)
            {
                MessageBox.Show("A data de fim não pode inferior à data de início");
            }
        }
        /// Interação lógica para a ação de abandono do campo Mail.
        private void tbMail_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            mailCorreto = 1;
            Regex regex = new Regex("^[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+(?:.[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+)*@[A-Za-z0-9-]+(?:.[A-Za-z0-9-]+)*$");
            if (!regex.IsMatch(tbMail.Text))
            {
                aviso = tbMail.Text + " - Não parece ser um endereço de mail válido.";
                MessageBox.Show(aviso);
                mailCorreto = 0;
            }
        }
        /// Interação lógica para a ação de abandono do campo Telefone.
        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            numCorreto = 1;
            TextBox textoRec = sender as TextBox;
            Regex regex = new Regex("^[0-9]{9}$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - Não é um valor válido, deve conter precisamente 9 dígitos de 0 a 9, sem espaços.";
                MessageBox.Show(aviso);
                numCorreto = 0;
            }
        }
        /// Interação lógica para a ação de abandono de qualquer um dos campos de minutos.
        private void min_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis correspondentes a 1 para o caso de ter sido colocada a 0.
            if (textoRec.Name == "tbMinutoIn")
            { minInCorreto = 1; }
            else
            { minOutCorreto = 1; }

            Regex regex = new Regex("^[0-5][0-9]$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - O valor para minutos deve estar entre 00 e 59.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbMinutoIn")
                { minInCorreto = 0; }
                else
                { minOutCorreto = 0; }
            }
        }
        /// Interação lógica para a ação de abandono de qualquer um dos campos de hora.
        private void hora_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            TextBox textoRec = sender as TextBox;
            // Reforço da colocação das variáveis correspondentes a 1 para o caso de ter sido colocada a 0.
            if (textoRec.Name == "tbHoraIn")
            { horaInCorreto = 1; }
            else
            { horaOutCorreto = 1; }
            Regex regex = new Regex("^[0-1][0-9]|2[0-3]$");
            if (!regex.IsMatch(textoRec.Text))
            {
                aviso = textoRec.Text + " - O valor para horas deve estar entre 00 e 23.";
                MessageBox.Show(aviso);
                if (textoRec.Name == "tbHoraIn")
                { horaInCorreto = 0; }
                else
                { horaOutCorreto = 0; }
            }
        }
    }
}
