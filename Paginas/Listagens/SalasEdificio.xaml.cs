﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using MessageBox = System.Windows.Forms.MessageBox;

namespace GesSal.Paginas.Listagens
{
    /// <summary>
    /// Interação lógica para SalasEdificio.xaml
    /// </summary>
    public partial class SalasEdificio : Page
    {

        // Criação de uma variável para confirmar a seleção do utilizador.
        int selecao = 0;
        string edif = "";

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public SalasEdificio()
        {
            InitializeComponent();

            using (GesSalEntities sacaDados = new GesSalEntities())
            {

                // Atribuição da lista á combobox.
                var qEdificio = (from c in sacaDados.edificios select c).ToList();
                cbEdificio.ItemsSource = qEdificio;
            }
        }


        /// <summary>
        /// Interação lógica para a ação de selecionar um registo da combobox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbEdificio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (GesSalEntities sacaDados = new GesSalEntities())
            {

                // Criação de variáveis para recolher a seleção do utilizador.
                selecao = ((edificios)cbEdificio.SelectedValue).idEdificio;
                edif = ((edificios)cbEdificio.SelectedValue).idetificaco;

                //Criação de uma variável para receber os dados da query feita.
                var qSala = (from c in sacaDados.salas
                             where c.idEdificio.ToString().Contains(selecao.ToString())
                             select c).ToList();

                // Passagem dos dados para a datagrid.
                dgDados.ItemsSource = qSala;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "PDF".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butPDF_Click(object sender, RoutedEventArgs e)
        {


            // Verificação de se existe alguma seleção efetiva.
            if (selecao == 0)
            {
                MessageBox.Show("Tem primeiro se selecionar um edifício");
            }
            else
            {

                // Criação de um novo documento PDF.
                PdfDocument documento = new PdfDocument();

                // Atribuição de definições da página.
                documento.PageSettings.Orientation = PdfPageOrientation.Landscape;
                documento.PageSettings.Margins.All = 50;
                documento.PageSettings.Size = PdfPageSize.A4;

                // Adição da página ao documento
                PdfPage pagina = documento.Pages.Add();
                PdfGraphics grafico = pagina.Graphics;
                string caminho = "\\\\" + Principal.Janela.DominioGesSala + "\\GesSala\\Imagens\\Compo.png";
                PdfImage imagem = PdfImage.FromFile(caminho);

                //Colocação da imagem na página
                pagina.Graphics.DrawImage(imagem, new RectangleF(0, 0, 50, 50));

                //Criação e colocação do cabeçalho com informação do utilizador que criou a listagem  e a data.
                PdfLayoutResult result = new PdfLayoutResult(pagina, new RectangleF(0, -70, pagina.Graphics.ClientSize.Width / 2, 95));
                PdfFont subHeadingFont = new PdfStandardFont(PdfFontFamily.TimesRoman, 14);
                grafico.DrawRectangle(new PdfSolidBrush(new PdfColor(126, 151, 173)), new RectangleF(0, result.Bounds.Bottom + 40, grafico.ClientSize.Width, 30));
                PdfTextElement element = new PdfTextElement();

                element = new PdfTextElement("Utilizador: " + Principal.Janela.UtilizadorGesSala, subHeadingFont);
                element.Brush = PdfBrushes.White;
                result = element.Draw(pagina, new PointF(10, result.Bounds.Bottom + 45));

                string dataAtual = "Data: " + DateTime.Now.ToString("dd/MM/yyyy");
                SizeF textSize = subHeadingFont.MeasureString(dataAtual);
                grafico.DrawString(dataAtual, subHeadingFont, element.Brush, new PointF(grafico.ClientSize.Width - textSize.Width - 10, result.Bounds.Y));

                // Colocação do edifício selecionado no relatório.
                grafico.DrawString(" Edifício: " + edif, subHeadingFont, PdfBrushes.Black, new PointF(0, result.Bounds.Bottom + 15));


                // Colocação de uma linha de separação
                grafico.DrawLine(new PdfPen(new PdfColor(126, 151, 173), 0.70f), new PointF(0, result.Bounds.Bottom + 35), new PointF(grafico.ClientSize.Width, result.Bounds.Bottom + 35));

                // Criação da lista a passar para o documento.
                PdfGrid recebeLista = new PdfGrid();
                using (GesSalEntities sacaDados = new GesSalEntities())
                {
                    //// Variável para recolher a seleção do utilizador.
                    // selecao = ((edificios)cbEdificio.SelectedValue).idEdificio;

                    //Criação de uma variável para receber os dados da query feita.
                    var qSala = (from c in sacaDados.salas
                                 where c.idEdificio.ToString().Contains(selecao.ToString())
                                 select new
                                 {
                                    ID = c.idSala,
                                    Nome = c.identificacao,
                                    Piso = c.piso,
                                    Largura = c.largura,
                                    Altura = c.altura,
                                    Comprimento = c.comprimento,
                                    Características = c.caracteristicas
                                 }).ToList();

                    // Passagem dos dados para a lista criada
                    recebeLista.DataSource = qSala;
                }


                // Configuração dos estilos das células.
                PdfGridCellStyle cellStyle = new PdfGridCellStyle();
                cellStyle.Borders.All = PdfPens.DarkMagenta;
                PdfGridRow header = recebeLista.Headers[0];

                // Configuração dos estilos do cabeçalho.
                PdfGridCellStyle headerStyle = new PdfGridCellStyle();
                headerStyle.Borders.All = new PdfPen(new PdfColor(126, 151, 173));
                headerStyle.BackgroundBrush = new PdfSolidBrush(new PdfColor(126, 151, 173));
                headerStyle.TextBrush = PdfBrushes.White;
                headerStyle.Font = new PdfStandardFont(PdfFontFamily.TimesRoman, 12f, PdfFontStyle.Regular);

                // Criação das colunas da tabela.
                for (int i = 0; i < header.Cells.Count; i++)
                {
                    header.Cells[i].StringFormat = new PdfStringFormat(PdfTextAlignment.Center, PdfVerticalAlignment.Middle);
                }

                // Alpicação dos vários estilos à criação da tabela. 
                header.ApplyStyle(headerStyle);
                cellStyle.Borders.Bottom = new PdfPen(new PdfColor(217, 217, 217), 0.70f);
                cellStyle.Font = new PdfStandardFont(PdfFontFamily.TimesRoman, 12f);
                cellStyle.TextBrush = new PdfSolidBrush(new PdfColor(131, 130, 136));
                PdfGridLayoutFormat layoutFormat = new PdfGridLayoutFormat();
                layoutFormat.Layout = PdfLayoutType.Paginate;

                // Criação da tabela com os dados obtidos
                PdfGridLayoutResult gridResult = recebeLista.Draw(pagina, new RectangleF(new PointF(0, result.Bounds.Bottom + 40), new SizeF(grafico.ClientSize.Width, grafico.ClientSize.Height - 100)), layoutFormat);

                //Guardar o documento criado num ficheiro.
                SaveFileDialog saveFiles = new SaveFileDialog();
                saveFiles.Filter = "Ficheiros PDF (*.PDF)|*.PDF";
                saveFiles.ShowDialog();
                documento.Save(saveFiles.FileName);
            }
        }
    }
}
