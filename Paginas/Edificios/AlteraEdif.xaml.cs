﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace GesSal.Paginas.Edificios
{
    /// <summary>
    /// Interação lógica para AlteraEdif.xaml
    /// </summary>
    public partial class AlteraEdif : Page
    {
        // Criação de variável para guardar cliente selecionado.
        int selecao = VerEdif.JanEdif.numOrd[VerEdif.JanEdif.indiceAVista];

        // Criação de variáveis para verificação final de alguns campos, se estão
        // preenchidos corretamente, são inicializados a 1 pelo facto de alguns deles 
        // não serem de preenchimento obrigatório.
        int numCorreto, mailCorreto, cPostalCorreto = 1;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public AlteraEdif()
        {
            // Inicialização dos componentes desta classe.
            InitializeComponent();

            // Pesquisa e preenchimento dos campos de texto correspondentes.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var edifSelecionado = (from c in sacaDados.edificios
                                       where c.idEdificio == selecao
                                       select c).FirstOrDefault();

                tbIdentificacao.Text = edifSelecionado.idetificaco;
                tbContacto.Text = edifSelecionado.contacto;
                tbTelefone.Text = edifSelecionado.telefone;
                tbMail.Text = edifSelecionado.eMail;
                tbMorada.Text = edifSelecionado.morada;
                tbCPostal.Text = edifSelecionado.codigoPostal;
                tbLocalidade.Text = edifSelecionado.localidade;
                tbConcelho.Text = edifSelecionado.concelho;
                tbDistrito.Text = edifSelecionado.distrito;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerEdif();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butOk_Click(object sender, RoutedEventArgs e)
        {
            // Verificação de se os campos obrigatórios estão preenchidos
            if (tbConcelho.Text == "" ||
                tbDistrito.Text == "" ||
                tbLocalidade.Text == "" ||
                tbMorada.Text == "" ||
                tbIdentificacao.Text == "" ||
                tbCPostal.Text == "")
            {
                MessageBox.Show("Os campos assinalados com \"(*)\" são de preenchimento obrigatório");
            }
            // Verificação de se algums campos estão preenchidos corretamente.
            else if (numCorreto + mailCorreto + cPostalCorreto < 3)
            {
                MessageBox.Show("Algum dos campos preenchidos não contém informação válida, deve fazer uma verificação mais aprofundada.");
            }
            else
            {
                using (GesSalEntities sacaDados = new GesSalEntities())
                {
                    List<edificios> qEdificio;
                    qEdificio = (from c in sacaDados.edificios
                                 where (c.idetificaco.Contains(tbIdentificacao.Text))
                                 select c).ToList();

                    // Verificação de se existe algum edifício com o mesmo nome introduzido.
                    if (qEdificio.Count > 0)
                    {
                        string aviso = "O edifício com o nome " + tbIdentificacao.Text + " já existe.";
                        MessageBox.Show(aviso);
                    }
                    else
                    {
                        edificios meuEdif = sacaDados.edificios.Where(x => x.idEdificio == selecao).FirstOrDefault();
                        meuEdif.idetificaco = tbIdentificacao.Text;
                        meuEdif.contacto = tbContacto.Text;
                        meuEdif.telefone = tbTelefone.Text;
                        meuEdif.eMail = tbMail.Text;
                        meuEdif.morada = tbMorada.Text;
                        meuEdif.codigoPostal = tbCPostal.Text;
                        meuEdif.localidade = tbLocalidade.Text;
                        meuEdif.concelho = tbConcelho.Text;
                        meuEdif.distrito = tbDistrito.Text;
                        sacaDados.SaveChanges();
                        Principal.Janela.AreaTrabalho.Content = new VerEdif();
                    }
                }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Eliminar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Pretende mesmo eliminar este Edifício?", "A Eliminar...", MessageBoxButton.YesNo).ToString() == "Yes")
            {
                using (GesSalEntities db = new GesSalEntities())
                {
                    List<salas> qSala;
                    qSala = (from c in db.salas
                               where (c.idEdificio.ToString().Contains(selecao.ToString()))
                               select c).ToList();

                    // Verificação de se existe algum evento que o cliente tenha organizado.
                    if (qSala.Count > 0)
                    {
                        string aviso = "O edifício com o nome " + tbIdentificacao.Text + " não pode ser eliminado pois tem salas atribuidas.";
                        MessageBox.Show(aviso);
                    }
                    else
                    {
                        edificios removeEdificio = db.edificios.Where(x => x.idEdificio == selecao).FirstOrDefault();
                        db.edificios.Remove(removeEdificio);
                        db.SaveChanges();
                    }
                }
            }
            Principal.Janela.AreaTrabalho.Content = new VerEdif();
        }

        // Verificação do preenchimento de alguns campos, se estão na configuração devida ou não, 
        // com a emissão do respetivo aviso. São utilizdas expressões regulares para o efeito
        // sendo o campo verificado logo após o preenchimento.

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Telefone.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbTelefone_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            numCorreto = 1;
            Regex regex = new Regex("^[0-9]{9}$");
            if (!regex.IsMatch(tbTelefone.Text))
            {
                aviso = tbTelefone.Text + " - Não é um valor válido, deve conter precisamente 9 dígitos de 0 a 9, sem espaços.";
                MessageBox.Show(aviso);
                numCorreto = tbTelefone.Text == "" ? 1 : 0;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Mail.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbMail_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            mailCorreto = 1;
            Regex regex = new Regex("^[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+(?:.[A-Za-z0-9_!#$%&'*+/=?`{|}~^-]+)*@[A-Za-z0-9-]+(?:.[A-Za-z0-9-]+)*$");
            if (!regex.IsMatch(tbMail.Text))
            {
                aviso = tbMail.Text + " - Não parece ser um endereço de mail válido.";
                MessageBox.Show(aviso);
                mailCorreto = tbMail.Text == "" ? 1 : 0;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de abandono do campo Código Postal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbCPostal_LostFocus(object sender, RoutedEventArgs e)
        {
            string aviso;
            // Reforço da colocação da variável a 1 para o caso de ter sido colocada a 0.
            cPostalCorreto = 1;
            Regex regex = new Regex("^[0-9]{4}-[0-9]{3}$");
            if (!regex.IsMatch(tbCPostal.Text))
            {
                aviso = tbCPostal.Text + " - Não é um Código Postal válido, deve inserir no formato XXXX-XXX.";
                MessageBox.Show(aviso);
                cPostalCorreto = tbCPostal.Text == "" ? 1 : 0;
            }
        }
    }
}
