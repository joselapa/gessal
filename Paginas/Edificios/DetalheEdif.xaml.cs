﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Edificios
{
    /// <summary>
    /// Interação lógica para DetalheEdif.xaml
    /// </summary>
    public partial class DetalheEdif : Page
    {

        // Criação de variável para guardar cliente selecionado.
        int selecao = VerEdif.JanEdif.numOrd[VerEdif.JanEdif.indiceAVista];

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public DetalheEdif()
        {
            // Inicialização dos componentes desta classe.
            InitializeComponent();

            // Pesquisa e preenchimento das labels e grids correspondentes.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var edifSelecionado = (from c in sacaDados.edificios
                                       where c.idEdificio == selecao
                                       select c).FirstOrDefault();
                gbEdificio.Content = edifSelecionado.idetificaco;

                var qSalas = (from c in sacaDados.salas
                              where c.idEdificio == selecao
                              select c).ToList();
                dgDados.ItemsSource = qSalas;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Ok".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butOk_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new VerEdif();
        }
    }


}
