﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Edificios
{
    /// <summary>
    /// Interação lógica para PesquEdif.xaml
    /// </summary>
    public partial class PesquEdif : Window
    {
        // Criação de variável que serve para guardar o edifício selecionado para passar 
        // no final ao user interface de Ver Cliente.
        int verSelecao = 0;

        // Criação de uma lista para preservar a pesquisa feita nesta janela.
        List<edificios> qEdif;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public PesquEdif()
        {
            InitializeComponent();

            // Aplicação da query à base de dados para iniciar a lista atrás criada
            // e apresenta-la no dataGrid correspondente.
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                qEdif = (from c in sacaDados.edificios select c).ToList();
                dgDados.ItemsSource = qEdif;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Selecionar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butSelecionar_Click(object sender, RoutedEventArgs e)
        {
            if (verSelecao != 0)
            {
                // Passagem dos valores da seleção feita para as variáveis do user interface correspondente.
                VerEdif.JanEdif.numOrd = (from c in qEdif select c.idEdificio).ToList();
                VerEdif.JanEdif.indiceAVista = VerEdif.JanEdif.numOrd.FindIndex(x => x == verSelecao);
                VerEdif.JanEdif.PassaParaUI(VerEdif.JanEdif.numOrd[VerEdif.JanEdif.indiceAVista]);
                Close();
            }
            else
            {
                MessageBox.Show("Tem de primeiro selecionar um Edifício!");
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Cancelar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Interação lógica para a ação de alteração de seleção no elemento da DataGrid
        /// atualizando a variável de verSeleção com o valor correspondente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgDados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;

            if (dataGrid.CurrentItem != null && e.AddedItems.Count > 0)
            {
                verSelecao = ((edificios)dataGrid.CurrentItem).idEdificio;
            }
            else
            {
                verSelecao = 0;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de escrever nos campos apresentados para criar a 
        /// lista de pesquisa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            // Instanciação DataGrid para apresentar a lista dos resultados apresentados.
            DataGrid dataGrid = sender as DataGrid;

            // Implementação da query onde se escreve o que se pretende pesquisar.
            using (GesSalEntities baseDados = new GesSalEntities())
            {
                qEdif = (from c in baseDados.edificios
                         where (c.idetificaco.Contains(tbIdentificacao.Text) && c.morada.Contains(tbMorada.Text))
                         select c).ToList();
                dgDados.ItemsSource = qEdif;
            }

        }


    }
}
