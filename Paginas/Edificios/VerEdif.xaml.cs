﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GesSal.Paginas.Edificios
{
    /// <summary>
    /// Interação lógica para VerEdif.xaml
    /// </summary>
    public partial class VerEdif : Page
    {

        // Criação de uma variável estática do tipo desta classe para conseguir  
        // ter acesso às propriedades publicas desta classe noutras classes 
        // desta aplicação.
        public static VerEdif JanEdif;
        
        // Implementação de uma lista para receber um conjunto de indices de clientes
        // com a finalidade de utilizar os botões "Seguinte" e "Anterior" para ser posicionado 
        // os dados do edifício mais conveniente da últma pesquisa.
        public List<int> numOrd = new List<int>();

        // Implementação de uma váriável para identificar o cliente que está atulamente à vista.
        public int indiceAVista = 0;

        /// <summary>
        /// Implementação do método construtor desta classe.
        /// </summary>
        public VerEdif()
        {
            //Inicialização da variável estática criada atrás.
            JanEdif = this;
            InitializeComponent();

            // Criação de uma primeira lista para colocar em funcionamento
            // os botões de "Seguinte" e "Anterior".
            CriaIndice("", "");

            // Preenchimento de primeiros valores ceum cliente nos campos à vista.
            PassaParaUI(numOrd[indiceAVista]);
        }


        /// <summary>
        /// Método a aplicar a query para a criação da priemira lista.
        /// </summary>
        /// <param name="ident"></param>
        /// <param name="morad"></param>
        private void CriaIndice(string ident, string morad)
        {
            using (GesSalEntities baseDados = new GesSalEntities())
            {
                numOrd = (from c in baseDados.edificios
                          where (c.idetificaco.Contains(ident) && c.morada.Contains(morad))
                          select c.idEdificio).ToList();
            }
        }

        /// <summary>
        /// Método para o preenchimento de valores nos campos apresentados com uma lista 
        /// pretendida.
        /// </summary>
        /// <param name="x"></param>
        public void PassaParaUI(int x)
        {
            using (GesSalEntities sacaDados = new GesSalEntities())
            {
                var edifSelecionado = (from c in sacaDados.edificios
                                       where c.idEdificio >= x
                                       select c).FirstOrDefault();

                gbIndentificacao.Content = edifSelecionado.idetificaco;
                gbContacto.Content = edifSelecionado.contacto;
                gbTelefone.Content = edifSelecionado.telefone;
                gbMail.Content = edifSelecionado.eMail;
                gbMorada.Content = edifSelecionado.morada;
                gbCPostal.Content = edifSelecionado.codigoPostal;
                gbLocalidade.Content = edifSelecionado.localidade;
                gbConcelho.Content = edifSelecionado.concelho;
                gbDistrito.Content = edifSelecionado.distrito;
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Criar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCriar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new CriarEdif();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Alterar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butAlterar_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new AlteraEdif();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Anterior".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butAnterior_Click(object sender, RoutedEventArgs e)
        {
            if (indiceAVista - 1 >= 0)
            {
                indiceAVista--;
                PassaParaUI(numOrd[indiceAVista]);
            }
        }

        /// <summary>
        ///  Interação lógica para a ação de clicar no botão "Seguinte".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butSeguinte_Click(object sender, RoutedEventArgs e)
        {
            if (indiceAVista + 1 < numOrd.Count)
            {
                indiceAVista++;
                PassaParaUI(numOrd[indiceAVista]);
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Detalhes".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butDetalhes_Click(object sender, RoutedEventArgs e)
        {
            Principal.Janela.AreaTrabalho.Content = new DetalheEdif();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão "Pesquisar".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butPesquisar_Click(object sender, RoutedEventArgs e)
        {
            PesquEdif JanPesq = new PesquEdif();
            JanPesq.ShowDialog();
        }
    }
}
