﻿using System;
using System.Windows;

namespace GesSal
{
    /// <summary>
    /// Interação lógica para Principal.xaml
    /// </summary>
    public partial class Principal : Window
    {
        // Criação de um variável estática do tipo desta classe para conseguir  
        // ter acesso às propriedades publicas desta classe noutras classes 
        // desta aplicação.
        public static Principal Janela;

        // Criação de variáveis para guardar o dominio e o utilizador obtidas na 
        // janela de login.
        public string UtilizadorGesSala, DominioGesSala;

        public Principal()
        {
            InitializeComponent();

            //Inicialização da variável com esta classe.
            Janela = this;

            // Inicialização das variáveis para guardar o dominio e o utilizador.
            UtilizadorGesSala = MainWindow.entrada.tbUtilizador.Text.ToString();
            DominioGesSala = MainWindow.entrada.gbDominio.Content.ToString();
            //Preenchimento da label com a página de entrada.
            AreaTrabalho.Content = new Entrada();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para ver clientes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientesVer_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Clientes.VerCliente();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para criar clientes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientesCriar_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Clientes.CriarCliente();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para ver Edifícios.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EdificiosVer_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Edificios.VerEdif();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para criar Edifícios.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EdificiosCriar_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Edificios.CriarEdif();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para ver Salas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SalasVer_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Salas.VerSala();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para criar Salas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SalasCriar_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Salas.CriarSala();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para ver Equipamentos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EquipVer_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Equipamentos.VerEquip();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para criar Equipamentos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EquipCriar_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Equipamentos.CriarEquip();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para ver Agenda.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AgendaVer_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Agenda.VerAgenda();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para criar Agenda.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AgendaCriar_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Agenda.CriarAgenda();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para emitir relação de salas por edifício.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListSalasEdif_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Listagens.SalasEdificio();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para emitir relação de eventos por cliente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListEventCli_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Listagens.EventosCliente();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para emitir relação de eventos por sala.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListEventSala_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Listagens.EventosSala();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para emitir relação de equipamentos por sala.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListEquipSala_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Paginas.Listagens.EquipamentosSala();
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para redirecionar de volta ao ecrâ inicial.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MnEntrada_Click(object sender, RoutedEventArgs e)
        {
            AreaTrabalho.Content = new Entrada();
        }
        
        /// <summary>
        /// Interação lógica para a ação de clicar no Menu para sair concluir a instância.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MnSair_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
