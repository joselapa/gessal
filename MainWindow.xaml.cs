﻿using System;
using System.Diagnostics;
using System.Windows;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace GesSal
{
    /// <summary>
    /// Interação lógica para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Criação de um variável estática do tipo desta classe para conseguir  
        // ter acesso aos elementos publicos desta classe nas outras classes 
        // parciais correspondestes às páginas de user interface.
        // Neste caso a finalidade corresponde à passagem da informaçãp de quem é
        // que se autenticou.
        public static MainWindow entrada;

        public MainWindow()
        {
            // Inicialização da variável com esta classe.
            entrada = this;

            // Este if é para verificar se já não existe outra instância desta classe, para garantir que 
            // apenas existe uma instância desta classe.
            if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
            {
                // Acão para quando foi detetado uma instância ativa, provocando o términus da instância.
                MessageBox.Show("O GesSal já se encontra em atividade.", "Programa ativo");
                Application.Current.Shutdown();
            }

            else
            {
                // Não havendo nenhuma instânciaa ativa procede com a inicialização dos componentes.
                InitializeComponent();
            }

            // Apresentação do dominio na label correspondente.
            try
            {
                // Situação para caso se tenha encontrado o dominio. 
                // Criação da uma instância para pode recolher as propriedades do RootDSE.
                DirectoryEntry entradaDominio = new DirectoryEntry("LDAP://RootDSE");


                // Recolha da propriedade do RootDSE que contém a informação do servidor de DNS
                // e colocá-la no espaço cotrrespondente.
                gbDominio.Content = entradaDominio.Properties["dnsHostName"][0].ToString();

            }
            catch
            {
                // situação para caso o sitema esteja em workgroup ou stand alone, que apresenta o nome do sistema.
                gbDominio.Content = Environment.MachineName.ToString();
            }
            
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão Entrar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btEntrar_Click(object sender, RoutedEventArgs e)
        {
            // Inicialização de uma variável que vai servir de base para a passagem para a janela seguinte.
            bool autoriza = false;

            // Verificação do conteúdo das caixas de texto, obrigando ao preenchimento em caso de estarem vazias.
            if (passBox.Password == "")
            {
                MessageBox.Show("A password não pode ser vazia, terá de consultar o seu gestor de serviços!","Password anómala");
            }
            else
            {
                // Este Try-Catch é para definir a utilização do programa tanto em dominio como em workgroup.
                try
                {

                    // Autenticação para uma situação de sistema em Active Directory.
                    DirectoryEntry entradaDominio = new DirectoryEntry("LDAP://" + gbDominio.Content.ToString(), tbUtilizador.Text, passBox.Password);
                    DirectorySearcher procuraDominio = new DirectorySearcher(entradaDominio);
                    SearchResult resultados = null;
                    resultados = procuraDominio.FindOne();
                    autoriza = true;
                }
                catch
                {
                    // Autenticação para uma situação de sistema em Security Acount Mamnagement.
                    PrincipalContext contextoPrincipal = new PrincipalContext(ContextType.Machine, gbDominio.Content.ToString());
                    autoriza = contextoPrincipal.ValidateCredentials(tbUtilizador.Text, passBox.Password);
                }

                // Permissão de passagem à janela seguinte em caso da variável estar em true.

                if (autoriza == true)
                {
                    // Passagem para a janela Principal após autenticação com sucesso.
                    Principal JanPrincipal = new Principal();
                    JanPrincipal.Show();
                    Close();
                }
                else
                {
                    // Mensagem para falha de validação.
                    MessageBox.Show("Dados de utilizador e/ou password errados, tente outra vez.");
                }
            }
        }

        /// <summary>
        /// Interação lógica para a ação de clicar no botão Sair.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSair_Click(object sender, RoutedEventArgs e)
        {
            // Términus da instância.
            Close();
        }


    }

}
