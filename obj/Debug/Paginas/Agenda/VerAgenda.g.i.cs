﻿#pragma checksum "..\..\..\..\Paginas\Agenda\VerAgenda.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7509A5131E89EC78F2D6B43E3E07C833A363383F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using GesSal.Paginas.Agenda;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GesSal.Paginas.Agenda {
    
    
    /// <summary>
    /// VerAgenda
    /// </summary>
    public partial class VerAgenda : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 74 "..\..\..\..\Paginas\Agenda\VerAgenda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgDados;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\..\Paginas\Agenda\VerAgenda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butAlterar;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\..\Paginas\Agenda\VerAgenda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butCriar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GesSal;component/paginas/agenda/veragenda.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Paginas\Agenda\VerAgenda.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dgDados = ((System.Windows.Controls.DataGrid)(target));
            
            #line 83 "..\..\..\..\Paginas\Agenda\VerAgenda.xaml"
            this.dgDados.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgDados_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.butAlterar = ((System.Windows.Controls.Button)(target));
            
            #line 119 "..\..\..\..\Paginas\Agenda\VerAgenda.xaml"
            this.butAlterar.Click += new System.Windows.RoutedEventHandler(this.butAlterar_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.butCriar = ((System.Windows.Controls.Button)(target));
            
            #line 124 "..\..\..\..\Paginas\Agenda\VerAgenda.xaml"
            this.butCriar.Click += new System.Windows.RoutedEventHandler(this.butCriar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

