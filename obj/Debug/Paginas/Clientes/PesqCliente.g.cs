﻿#pragma checksum "..\..\..\..\Paginas\Clientes\PesqCliente.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "BF5759A5B4E936FCDA0E829AA6AAC33DD81733A8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using GesSal.Paginas.Clientes;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GesSal.Paginas.Clientes {
    
    
    /// <summary>
    /// PesqCliente
    /// </summary>
    public partial class PesqCliente : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 76 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNome;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNif;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbContacto;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gpResultado;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgDados;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butSelecionar;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butCancelar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GesSal;component/paginas/clientes/pesqcliente.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tbNome = ((System.Windows.Controls.TextBox)(target));
            
            #line 78 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
            this.tbNome.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tbNif = ((System.Windows.Controls.TextBox)(target));
            
            #line 90 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
            this.tbNif.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.tbContacto = ((System.Windows.Controls.TextBox)(target));
            
            #line 102 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
            this.tbContacto.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.gpResultado = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 5:
            this.dgDados = ((System.Windows.Controls.DataGrid)(target));
            
            #line 119 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
            this.dgDados.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgDados_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.butSelecionar = ((System.Windows.Controls.Button)(target));
            
            #line 144 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
            this.butSelecionar.Click += new System.Windows.RoutedEventHandler(this.butSelecionar_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.butCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 149 "..\..\..\..\Paginas\Clientes\PesqCliente.xaml"
            this.butCancelar.Click += new System.Windows.RoutedEventHandler(this.butCancelar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

