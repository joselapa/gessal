﻿#pragma checksum "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "B7521144CB72E9955CFB630300955A5AF374AD89"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GesSal.Paginas.Equipamentos {
    
    
    /// <summary>
    /// PesqEquip
    /// </summary>
    public partial class PesqEquip : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 71 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNome;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbLotacao;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbObservacoes;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gpResultado;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lbAgenda;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem Resultado1;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem Resultado2;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem Resultado3;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butSelecionar;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butCancelar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GesSal;component/paginas/equipamentos/pesqequip.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tbNome = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.tbLotacao = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.tbObservacoes = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.gpResultado = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 5:
            this.lbAgenda = ((System.Windows.Controls.ListBox)(target));
            return;
            case 6:
            this.Resultado1 = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 7:
            this.Resultado2 = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 8:
            this.Resultado3 = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 9:
            this.butSelecionar = ((System.Windows.Controls.Button)(target));
            
            #line 127 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
            this.butSelecionar.Click += new System.Windows.RoutedEventHandler(this.butSelecionar_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.butCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 132 "..\..\..\..\Paginas\Equipamentos\PesqEquip.xaml"
            this.butCancelar.Click += new System.Windows.RoutedEventHandler(this.butCancelar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

