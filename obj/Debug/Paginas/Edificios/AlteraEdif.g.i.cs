﻿#pragma checksum "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "CFEE82E1AB83A8511619568D25E65F66830BB623"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using GesSal.Paginas.Edificios;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GesSal.Paginas.Edificios {
    
    
    /// <summary>
    /// AlteraEdif
    /// </summary>
    public partial class AlteraEdif : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 76 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbIdentificacao;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbContacto;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbTelefone;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbMail;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbMorada;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbCPostal;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbLocalidade;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbConcelho;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbDistrito;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butOk;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butCancelar;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butEliminar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GesSal;component/paginas/edificios/alteraedif.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tbIdentificacao = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.tbContacto = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.tbTelefone = ((System.Windows.Controls.TextBox)(target));
            
            #line 92 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
            this.tbTelefone.LostFocus += new System.Windows.RoutedEventHandler(this.tbTelefone_LostFocus);
            
            #line default
            #line hidden
            return;
            case 4:
            this.tbMail = ((System.Windows.Controls.TextBox)(target));
            
            #line 100 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
            this.tbMail.LostFocus += new System.Windows.RoutedEventHandler(this.tbMail_LostFocus);
            
            #line default
            #line hidden
            return;
            case 5:
            this.tbMorada = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.tbCPostal = ((System.Windows.Controls.TextBox)(target));
            
            #line 117 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
            this.tbCPostal.LostFocus += new System.Windows.RoutedEventHandler(this.tbCPostal_LostFocus);
            
            #line default
            #line hidden
            return;
            case 7:
            this.tbLocalidade = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.tbConcelho = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.tbDistrito = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.butOk = ((System.Windows.Controls.Button)(target));
            
            #line 148 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
            this.butOk.Click += new System.Windows.RoutedEventHandler(this.butOk_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.butCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 153 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
            this.butCancelar.Click += new System.Windows.RoutedEventHandler(this.butCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.butEliminar = ((System.Windows.Controls.Button)(target));
            
            #line 159 "..\..\..\..\Paginas\Edificios\AlteraEdif.xaml"
            this.butEliminar.Click += new System.Windows.RoutedEventHandler(this.butEliminar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

