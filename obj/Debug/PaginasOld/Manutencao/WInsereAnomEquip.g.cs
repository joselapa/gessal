﻿#pragma checksum "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "969878113E42540C6317AB32E504DFC1FED9DAEE"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using GesSal;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GesSal {
    
    
    /// <summary>
    /// WInsereAnomEquip
    /// </summary>
    public partial class WInsereAnomEquip : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 47 "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btOk;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btCancela;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbCodEquip;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gbLocalizacao;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbQuemRep;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbDescricao;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GesSal;component/paginasold/manutencao/winsereanomequip.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btOk = ((System.Windows.Controls.Button)(target));
            return;
            case 2:
            this.btCancela = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\..\..\PaginasOld\Manutencao\WInsereAnomEquip.xaml"
            this.btCancela.Click += new System.Windows.RoutedEventHandler(this.btCancela_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.tbCodEquip = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.gbLocalizacao = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 5:
            this.tbQuemRep = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.tbDescricao = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

