﻿#pragma checksum "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C25A9A92C90ED271410802615470994B43203BBC"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using GesSal;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GesSal {
    
    
    /// <summary>
    /// CriarProposta
    /// </summary>
    public partial class CriarProposta : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 53 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTitulo;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lbSalas;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem Sala1;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem Sala2;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem Sala3;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btCriar;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btCancelar;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gbCliente;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gbDesconto;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblIliquido;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblIva;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTotal;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btAdSala;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btElSala;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btAdCliente;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GesSal;component/paginasold/propostas/criarproposta.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lblTitulo = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.lbSalas = ((System.Windows.Controls.ListBox)(target));
            return;
            case 3:
            this.Sala1 = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 4:
            this.Sala2 = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 5:
            this.Sala3 = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 6:
            this.btCriar = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.btCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 84 "..\..\..\..\PaginasOld\Propostas\CriarProposta.xaml"
            this.btCancelar.Click += new System.Windows.RoutedEventHandler(this.btCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.gbCliente = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 9:
            this.gbDesconto = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 10:
            this.lblIliquido = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lblIva = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.lblTotal = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.btAdSala = ((System.Windows.Controls.Button)(target));
            return;
            case 14:
            this.btElSala = ((System.Windows.Controls.Button)(target));
            return;
            case 15:
            this.btAdCliente = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

